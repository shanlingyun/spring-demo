package com.jmyd.jianmo.extension.obs;

import com.obs.services.model.DeleteObjectResult;
import com.obs.services.model.PutObjectResult;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface ObsFileService {

    /**
     * 上传文件流
     * @param path
     * @param inputStream
     * @return
     */
    public PutObjectResult upload(String path, InputStream inputStream);

    /**
     * PutObjectRequest上传文件流
     * @param objectKey
     * @param inputStream
     * @return
     */
    public PutObjectResult uploadPutObjectRequest(String objectKey, InputStream inputStream);


    /**
     * 删除文件
     * @param path
     * @return
     */
    public DeleteObjectResult delete(String path);


    /**
     * 上传文件流
     * @param url
     * @param path
     * @return
     */
    PutObjectResult upload(String url,String path );

    void fileCopy(String sourceobjectname,String destobjectname);


    /**
     *<p>列举已上传的段</p>
     *
     *  @author: jiayongliang
     *  @Date: 2022/3/11 9:18
     *
     */
    public List<Map<String,Object>> listParts(String uploadId);


    /**
     *<p>取消分片上传</p>
     *
     *  @author: jiayongliang
     *  @Date: 2022/3/11 11:02
     *
     */

    public int deleteMultipartUpload(String uploadId,String objectName);


    public String obsUpload(File files, String originalFilename, long fileLength);


}
