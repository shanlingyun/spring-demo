package com.jmyd.jianmo.extension.obs.config;

import com.obs.services.ObsClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class ObsBeanConfig {
    @Value("${obs.ak}")
    private String ak;
    @Value("${obs.sk}")
    private String sk;
    @Value("${obs.endpoint}")
    private String endPoint;

    @Bean
    public ObsClient getObsClient() {
        log.info("obs资源加载中。。。。");
        return new ObsClient(ak, sk, endPoint);
    }
}
