package com.jmyd.jianmo.extension.obs.impl;

import com.jmyd.jianmo.extension.obs.ObsFileService;
import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ObsFileServiceImpl implements ObsFileService {

    @Value("${obs.bucketName}")
    private String bucketName;

     @Resource
     private ObsClient obsClient;
    // 每个分片的大小，用于计算文件有多少个分片。单位为字节。
    static final long partSize = 10 * 1024 * 1024L;   //10 MB。

    /**
     * 上传文件流
     * @param path
     * @param inputStream
     * @return
     */
    @Override
    public PutObjectResult upload(String path, InputStream inputStream){
        if(path.indexOf("/")==0){
            path = path.substring(1);
        }
        return obsClient.putObject(bucketName, path, inputStream);
     }

    /**
     * 上传文件流
     * @param url
     * @param path
     * @return
     */
    @Override
    public PutObjectResult upload(String url,String path ){
        if(path.indexOf("/")==0){
            path = path.substring(1);
        }
        File file = new File(path);
        log.error("url:{},path:{}",url,path);
        try(InputStream inputStream = new FileInputStream(file);) {
            return obsClient.putObject(bucketName,url,inputStream);
        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }

    @Override
    public void fileCopy(String sourceobjectname,String destobjectname) {
        try {
            if(sourceobjectname.indexOf("/")==0){
                sourceobjectname = sourceobjectname.substring(1);
            }
            if(destobjectname.indexOf("/")==0){
                destobjectname = destobjectname.substring(1);
            }
            obsClient.copyObject(bucketName, sourceobjectname, bucketName, destobjectname);
        }catch (Exception e){
            log.error("sourceobjectname:{}",sourceobjectname);
            log.error(e.getMessage(),e);
        }
    }



    /**
     * PutObjectRequest上传文件流
     * @param objectKey
     * @param inputStream
     * @return
     */
    @Override
    public PutObjectResult uploadPutObjectRequest(String objectKey, InputStream inputStream){
        PutObjectRequest request = new PutObjectRequest();
        request.setBucketName(bucketName);
        request.setObjectKey(objectKey);
        request.setInput(inputStream);
        return obsClient.putObject(request);
    }

    /**
     * 删除文件
     *
     * @param path
     * @return
     */
    @Override
    public DeleteObjectResult delete(String path) {
        return obsClient.deleteObject(bucketName, path);
    }


    /**
     * <p>列举已上传的段</p>
     *
     * @param uploadId
     * @author: jiayongliang
     * @Date: 2022/3/11 9:18
     * @return
     */
    @Override
    public List<Map<String,Object>> listParts(String uploadId) {
        String objectName = "ces";
        //列举已上传的段，其中uploadId来自于initiateMultipartUpload
        ListPartsRequest request = new ListPartsRequest(bucketName, objectName);
        request.setUploadId(uploadId);
        ListPartsResult result = obsClient.listParts(request);
        List<Map<String,Object>> list = new ArrayList<>();
        for(Multipart part : result.getMultipartList()){
            Map<String,Object> map = new HashMap<>();
            // 分段号，上传时候指定
            map.put("partNumber",part.getPartNumber());
            // 段数据大小
            map.put("size",part.getSize());
            // 分段的ETag值
            map.put("etag",part.getEtag());
            // 段的最后上传时间
            map.put("lastModified",part.getLastModified());
            list.add(map);
        }
        return list;
    }

    /**
     * <p>取消分片上传</p>
     *
     * @param uploadId
     * @author: jiayongliang
     * @Date: 2022/3/11 11:02
     */
    @Override
    public int deleteMultipartUpload(String uploadId,String objectName) {

        AbortMultipartUploadRequest request = new AbortMultipartUploadRequest(bucketName,objectName, uploadId);

        HeaderResponse headerResponse = obsClient.abortMultipartUpload(request);
        // 获取取消状态
        return headerResponse.getStatusCode();
    }

    @Override
    public String obsUpload(File files, String originalFilename, long fileLength) {
// 计算需要上传的段数
        long partCount = fileLength % partSize == 0 ? fileLength / partSize : fileLength / partSize + 1;
        final List<PartEtag> partEtags = Collections.synchronizedList(new ArrayList<PartEtag>());
        // 初始化线程池
        ExecutorService executorService = Executors.newFixedThreadPool(20);
// 初始化分段上传任务
        InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, originalFilename);
        InitiateMultipartUploadResult result = obsClient.initiateMultipartUpload(request);
        final String uploadId = result.getUploadId();
        for (int i = 0; i < partCount; i++){
            // 分段在文件中的起始位置
            final long offset = i * partSize;
            // 分段大小
            final long currPartSize = (i + 1 == partCount) ? fileLength - offset : partSize;
            // 分段号
            final int partNumber = i + 1;
            executorService.execute(new Runnable(){
                @Override
                public void run() {
                    UploadPartRequest uploadPartRequest = new UploadPartRequest();
                    uploadPartRequest.setBucketName(bucketName);
                    uploadPartRequest.setObjectKey(originalFilename);
                    uploadPartRequest.setUploadId(uploadId);
                    uploadPartRequest.setFile(files);
                    uploadPartRequest.setPartSize(currPartSize);
                    uploadPartRequest.setOffset(offset);
                    uploadPartRequest.setPartNumber(partNumber);
                    UploadPartResult uploadPartResult;
                    try {
                        uploadPartResult = obsClient.uploadPart(uploadPartRequest);
                        log.info("Part#" + partNumber + " done\n");
                        partEtags.add(new PartEtag(uploadPartResult.getEtag(), uploadPartResult.getPartNumber()));
                    }catch (ObsException e){
                        e.printStackTrace();
                    }
                }
            });
        }
// 等待上传完成
        executorService.shutdown();
        while (!executorService.isTerminated()){
            try{
                executorService.awaitTermination(1, TimeUnit.SECONDS);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        // 合并段
        CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(bucketName, originalFilename, uploadId, partEtags);
        CompleteMultipartUploadResult completeMultipartUploadResult = obsClient.completeMultipartUpload(completeMultipartUploadRequest);
        return completeMultipartUploadResult.getObjectKey();
    }


    private long skipBytesFromStream(InputStream inputStream, long n) {
        long remaining = n;
// SKIP_BUFFER_SIZE is used to determine the size of skipBuffer
        int SKIP_BUFFER_SIZE = 2048;
// skipBuffer is initialized in skip(long), if needed.
        byte[] skipBuffer  = new byte[SKIP_BUFFER_SIZE];
        int nr = 0;
        byte[] localSkipBuffer = skipBuffer;
        if (n <= 0) {
            return 0;
        }
        while (remaining > 0) {
            try {
                    nr = inputStream.read(localSkipBuffer, 0,
                    (int) Math.min(SKIP_BUFFER_SIZE, remaining));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (nr < 0) {
                break;
            }
            remaining -= nr;
        }
        return n - remaining;
    }

}
