package com.jmyd.jianmo.extension.mq.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RabbitMqConfig {
    @Value("${spring.rabbitmq.port}")
    private Integer port;
    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.password}")
    private String password;
    @Value("${spring.rabbitmq.username}")
    private String username;
    @Value("${spring.rabbitmq.virtual-host}")
    private String virtualHost;
    @Value("${spring.rabbitmq.virtual-hostold}")
    private String virtualHostOld;

    @Bean("connectionFactory")
    @Primary
    public CachingConnectionFactory connectionFactory(){
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host,port);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        cachingConnectionFactory.setVirtualHost(virtualHost);
        return cachingConnectionFactory;
    }

    @Bean("rabbitTemplate")
    @Primary
    public RabbitTemplate rabbitTemplate(@Qualifier("connectionFactory") CachingConnectionFactory connectionFactory){
        return new RabbitTemplate(connectionFactory);
    }

    @Bean("connectionOldFactory")
    public CachingConnectionFactory connectionOldFactory(){
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host,port);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        cachingConnectionFactory.setVirtualHost(virtualHostOld);
        return cachingConnectionFactory;
    }

    @Bean("rabbitTemplateOld")
    public RabbitTemplate rabbitTemplateOld(@Qualifier("connectionOldFactory") CachingConnectionFactory connectionOldFactory){
        return new RabbitTemplate(connectionOldFactory);
    }

}
