package com.jmyd.jianmo.extension.mq.constant;


/**
 *<p>主题秀浏览量消息队列</p>
 *
 *  @author: jiayongliang
 *  @Date: 2022/3/7 15:57
 *
 */
public class ThemeMQConstant {

    //主题秀浏览量消息队列
    public static final String QUEUE_THEME_VIEW_COUNT="queue.theme.view.count";

}
