package com.jmyd.jianmo.extension.mq.constant;


/**
 *<p>编辑器版本浏览量</p>
 *
 *  @author: jiayongliang
 *  @Date: 2022/4/27 11:12
 *
 */
public class EditorVersionMqConstant {

    // 浏览量
    public static final String QUEUE_EDITOR_VERSION_COUNT = "queue.jianmo.editor.version.count";
}
