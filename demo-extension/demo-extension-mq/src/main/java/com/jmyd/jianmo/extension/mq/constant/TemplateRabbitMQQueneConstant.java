package com.jmyd.jianmo.extension.mq.constant;

/**
 *<p>模板消息队列</p>
 *
 *  @author: jiayongliang
 *  @Date: 2022/1/5 14:06
 *
 */
public class TemplateRabbitMQQueneConstant {

    // 浏览量
    public static final String QUEUE_TEMPLATE_VIEW_NUMBER = "queue.jianmo.template.view";

    // 使用量新增
    public static final String QUEUE_TEMPLATE_USE_NUMBER_ADD = "queue.jianmo.template.useAdd";

    // 使用量减少
    public static final String QUEUE_TEMPLATE_USE_NUMBER_REDUCE = "queue.jianmo.template.useReduce";

}
