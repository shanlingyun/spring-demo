package com.jmyd.jianmo.extension.mq.constant;

public class LrDimageMQqueneConstant {

    // 使用量新增
    public static final String QUEUE_LRDIM_USE_NUMBER_ADD = "queue.jianmo.lrDimage.useAdd";

    // 使用量减少
    public static final String QUEUE_LRDIM_USE_NUMBER_REDUCE = "queue.jianmo.lrDimage.useReduce";
}
