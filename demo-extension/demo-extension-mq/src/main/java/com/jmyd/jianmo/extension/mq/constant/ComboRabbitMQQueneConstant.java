package com.jmyd.jianmo.extension.mq.constant;

public class ComboRabbitMQQueneConstant {
    private ComboRabbitMQQueneConstant(){
        throw new IllegalStateException("ComboRabbitMQQueneConstant class");
    }
    //套餐包支付消息队列
    public static final String QUEUE_COMBO_ORDER_PAY ="queue.jianmo.combo.pay";
    //套餐包使用消息队列
    public static final String QUEUE_COMBO_ROLE="queue.jianmo.combo.package.role";
    //套餐包使用消息队列
    public static final String QUEUE_JIANMO_SCENE="queue.jianmo.scene.picture";
    //模型转换消息通知
    public static final String QUEUE_JIANMO_MODEL_CONVERT="queue.jianmo.model.convert.update";
    //模型转换路径更新消息通知
    public static final String QUEUE_JIANMO_MODEL_CONVERT_FILE="queue.jianmo.model.convert.fileupdate";
    //banner点击
    public static final String QUEUE_JIANMO_BANNER_CLICKNUM="queue.jianmo.banner.click";

    //模型转换工具消息通知
    public static final String QUEUE_JIANMO_MODEL_CONVERT_UTIL_ADD="queue.jianmo.model.convert.util.add";
    //模型转换工具优先消息通知
    public static final String QUEUE_JIANMO_MODEL_CONVERT_UTIL_SUPADD="queue.jianmo.model.convert.util.supadd";
    //模型转换工具进度更新
    public static final String QUEUE_JIANMO_MODEL_CONVERT_UTIL_UPDATE="queue.jianmo.model.convert.util.update";

    //模型增删查改对列
    public static final String QUEUE_JIANMO_MODEL_COUNT="queue.jianmo.model.count";

    //模型删除消息队列
    public static final String QUEUE_MODEL_DELETE="queue.jianmo.model.delete";

}
