package com.jmyd.jianmo.extension.mq.constant;

public class AspectRabbitMQQueneConstant {

    private AspectRabbitMQQueneConstant(){
        throw new IllegalStateException("AspectRabbitMQQueneConstant class");
    }
    //AOP切面操作拦截消息队列
    public static final String QUEUE_ASPECT_AOP_LOG = "queue.jianmo.system.aspect";
}

