package com.jmyd.jianmo.extension.redis.impl;

import com.jmyd.jianmo.extension.redis.RedisService;
import com.jmyd.jianmo.extension.redis.constant.RedisConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.types.Expiration;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class RedisServiceImpl implements RedisService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 获取String元素key值
     * @param key
     * @return
     */
    @Override
    public Object getRedisObject(String key){
         return redisTemplate
                 .opsForValue()
                 .get(key);
    }


    @Override
    public void setTemplateType(String templateTypeKey, String sceneTypeStr) {
       /* redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                //设置NX
                RedisStringCommands.SetOption setOption = RedisStringCommands.SetOption.ifAbsent();
                //设置过期时间
                Expiration expiration = Expiration.seconds(TimeUnit.HOURS.toSeconds(RedisConstant.CACHE_TIME_HOUR));
                //序列化key
                byte[] redisKey = redisTemplate.getKeySerializer().serialize(templateTypeKey);
                //序列化value
                byte[] redisValue = redisTemplate.getValueSerializer().serialize(sceneTypeStr);
                //执行setnx操作
                Boolean result = connection.set(redisKey, redisValue, expiration, setOption);
                return result;
            }
        });*/
        stringRedisTemplate.opsForValue().set(templateTypeKey,sceneTypeStr, RedisConstant.CACHE_TIME_HOUR, TimeUnit.HOURS);
    }

    @Override
    public void setTemplateType(String key, String object, Long time, TimeUnit timeUnit) {
        stringRedisTemplate.opsForValue().set(key,object, time, timeUnit);
    }

    @Override
    public String getTemplateType(String templateTypeKey) {
        return stringRedisTemplate.opsForValue().get(templateTypeKey);
    }

    @Override
    public Long getTemplateTypeTime(String templateTypeKey) {
        return stringRedisTemplate.getExpire(templateTypeKey);
    }

    @Override
    public void reSetTemplateTypeExpireTime(String templateTypeKey,Long timeout,TimeUnit timeUnit) {
          stringRedisTemplate.expire(templateTypeKey,timeout,timeUnit);
    }

    /**
     * 删除指定key
     * @param key
     */
    @Override
    public boolean deleteKey(String key) {
       return this.redisTemplate.delete(key);
    }

    /**
     * 设置string 指定key过期时间
     * @param key
     * @param timeout
     * @param timeUnit
     */
    @Override
    public void reSetKeyExpireTime(String key,Long timeout,TimeUnit timeUnit) {
        this.redisTemplate.expire(key,timeout,timeUnit);
    }

    @Override
    public Long getExpireTime(String templateTypeKey) {
        return redisTemplate.getExpire(templateTypeKey);
    }

    /**
     * 添加String元素
     * @param key
     * @param object
     * @param time
     * @param timeUnit
     */
    @Override
    public void setRedisObject(String key, Object object, Long time, TimeUnit timeUnit) {
        if(object==null){
            return;
        }
        redisTemplate
                .opsForValue()
                .set(key,object,time,timeUnit);
    }

    /**
     * 获取list集合所有值域
     * @param key
     * @return
     */
    @Override
    public Object getRedisList(String key) {
        return   redisTemplate.opsForValue().get(key);

       /* return this.redisTemplate
                .boundListOps(key)
                .range(0,-1);*/
    }
    /**
     * 获取list集合指定方位内所有值域
     * @param key
     * @param start
     * @param end
     * @return
     */
    @Override
    public List getRedisList(String key, Long start, Long end) {
        return this.redisTemplate
                .boundListOps(key)
                .range(start,end);
    }

    /**
     * 添加List集合
     * @param key
     * @param dataList
     * @param time
     * @param timeUnit
     */
    @Override
    public void setRedisList(String key, List dataList, Long time, TimeUnit timeUnit) {
        if(dataList==null ||dataList.isEmpty()){
            return;
        }
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                //设置NX
                RedisStringCommands.SetOption setOption = RedisStringCommands.SetOption.ifAbsent();
                //设置过期时间
                Expiration expiration = Expiration.seconds(TimeUnit.HOURS.toSeconds(RedisConstant.CACHE_TIME_HOUR));
                //序列化key
                byte[] redisKey = redisTemplate.getKeySerializer().serialize(key);
                //序列化value
                byte[] redisValue = redisTemplate.getValueSerializer().serialize(dataList);
                //执行setnx操作
                Boolean result = connection.set(redisKey, redisValue, expiration, setOption);
                return result;
            }
        });
       /* this.redisTemplate
                .boundListOps(key)
                .rightPushAll(dataList.toArray());
        this.redisTemplate
                .boundListOps(key)
                .expire(time,timeUnit);*/
    }
    @Override
    public void setSyncRedisList(String key, List dataList, Long time, TimeUnit timeUnit) {
        if(dataList==null ||dataList.isEmpty()){
            return;
        }
     /*   this.redisTemplate
                .boundListOps(key)
                .rightPushAll(dataList.toArray());
        this.redisTemplate
                .boundListOps(key)
                .expire(time,timeUnit);*/

        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                //设置NX
                RedisStringCommands.SetOption setOption = RedisStringCommands.SetOption.ifAbsent();
                //设置过期时间
                Expiration expiration = Expiration.seconds(TimeUnit.HOURS.toSeconds(RedisConstant.CACHE_TIME_HOUR));
                //序列化key
                byte[] redisKey = redisTemplate.getKeySerializer().serialize(key);
                //序列化value
                byte[] redisValue = redisTemplate.getValueSerializer().serialize(dataList);
                //执行setnx操作
                Boolean result = connection.set(redisKey, redisValue, expiration, setOption);
                return result;
            }
        });
    }
    /**
     * 获取list集合长度
     * @param key
     * @return
     */
    @Override
    public Long getListSize(String key) {
        return this.redisTemplate
                .boundListOps(key)
                .size();
    }

    /**
     * 设置zset指定元素
     * @param key
     * @param value
     * @param score
     * @param time
     * @param timeUnit
     */
    @Override
    public void setZSetValue(String key, Object value,double score, Long time, TimeUnit timeUnit) {
        BoundZSetOperations zSetOperations =this.redisTemplate.boundZSetOps(key);
        zSetOperations.add(value,score);
        zSetOperations.expire(time,timeUnit);
    }

    /**
     * 获得zset指定元素的所有集合
     * @param key
     * @return
     */
    @Override
    public Set<String> getZSetValues(String key) {
        return redisTemplate
                .boundZSetOps(key)
                .range(0,-1);
    }

    /**
     * 获得指定元素的分数
     * @param key
     * @param zSetValue
     * @return
     */
    @Override
    public Double getZSetScore(String key,String zSetValue) {
        return redisTemplate
                .boundZSetOps(key)
                .score(zSetValue);
    }

    /**
     * 获取zset指定元素长度
     * @param key
     * @return
     */
    @Override
    public Long getZSetSize(String key) {
        return redisTemplate
                .boundZSetOps(key)
                .size();
    }
    /**
     * 获得zset指定元素的得分范围内起始和结束的范围所有集合
     * @param key
     * @param minScore
     * @param maxScore
     * @param start
     * @param end
     * @return
     */
    @Override
    public Set<String> getZSetValues(String key, Double minScore, Double maxScore, Integer start, Integer end) {
        return redisTemplate
                .opsForZSet()
                .rangeByScore(key,minScore,maxScore,start,end);
    }

    /**
     * 删除zset指定值元素
     * @param key
     * @param value
     */
    @Override
    public Long zSetRemoveByValue(String key,Object value) {
        return redisTemplate
                .opsForZSet()
                .remove(key,value);
    }

    /**
     * 删除zset指定分数范围内元素
     * @param key
     * @param minScore
     * @param maxScore
     */
    @Override
    public Long zSetRemoveByScore(String key,Double minScore, Double maxScore) {
        return redisTemplate
                .opsForZSet()
                .removeRangeByScore(key,minScore,maxScore);
    }
    /**
     * 删除zset指定范围内元素
     * @param key
     * @param start
     * @param end
     */
    @Override
    public Long zSetRemoveByRang(String key,Long start, Long end) {
        return redisTemplate
                .opsForZSet()
                .removeRange(key,start,end);
    }

}
