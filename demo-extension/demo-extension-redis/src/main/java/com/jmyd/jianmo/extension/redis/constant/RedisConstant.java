package com.jmyd.jianmo.extension.redis.constant;

public class RedisConstant {

    // 存放后台用户登录token信息
    public static final String ADMIN_USER_TOKEN="jianmo:admin:user:token:";
    //超级用户token键值
    public static final String SUPER_ADMIN_TOKEN="jianmo:super:admin:token:";
    //普通缓存2小时
    public  static final Long CACHE_TIME_HOUR=2L;

    //普通缓存10分钟
    public  static final Long CACHE_TIME_MINUTE=10L;

    //普通缓存30秒
    public  static final Long CACHE_TIME_SECONDS=30L;

    //存放用户登录ey值
    public static final String USER_TOKEN="jianmo:api:user:token:";
    //用户登录账号key值
    public static final String API_USER_TOKEN_LOGIN ="jianmo:api:user:login:";

    //API缓存前缀
    public static final String API_REDIS_PREFIX_KEY ="jianmo:api:%s";

    public  static final String MODEL_ADMIN_TYPE_KEY = "jianmo:admin:modelType:";

    public  static final String ADMIN_REDIS_PREFIX_KEY = "jianmo:admin:%s:%s";

    public  static final String ADMIN_USER_MODEL_CONVERT_COUNT = "jianmo:admin:model:convert:";

}
