package com.jmyd.jianmo.extension.redis;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface RedisService {


    void setTemplateType(String templateTypeKey, String sceneTypeStr);


        void setTemplateType(String key, String str, Long time, TimeUnit timeUnit);


    /**
     * 获取String元素key值
     * @param key
     * @return
     */
    Object getRedisObject(String key);

     String getTemplateType(String templateTypeKey);

    /**
     * 添加String元素
     * @param key
     * @param object
     * @param time
     * @param timeUnit
     */
    void setRedisObject(String key, Object object, Long time, TimeUnit timeUnit);

    /**
     * 删除指定key
     * @param key
     */
    boolean deleteKey(String key);


    /**
     * 设置指定key过期时间
     * @param key
     * @param timeout
     * @param timeUnit
     */
    void reSetKeyExpireTime(String key,Long timeout,TimeUnit timeUnit);

    Long getExpireTime(String templateTypeKey);

        /**
         * 获取list集合所有值域
         * @param key
         * @return
         */
        Object getRedisList(String key);

    /**
     * 获取list集合指定方位内所有值域
     * @param key
     * @param start
     * @param end
     * @return
     */
    List getRedisList(String key,Long start,Long end);

    /**
     * 添加List集合
     * @param key
     * @param dataList
     * @param time
     * @param timeUnit
     */
    void setRedisList(String key, List dataList, Long time, TimeUnit timeUnit);

    void setSyncRedisList(String key, List dataList, Long time, TimeUnit timeUnit);
        /**
         * 获取list集合长度
         * @param key
         * @return
         */
    Long getListSize(String key);


    /**
     * 设置zset指定元素
     * @param key
     * @param value
     * @param score
     * @param time
     * @param timeUnit
     */
    void setZSetValue(String key,Object value,double score,Long time,TimeUnit timeUnit);

    /**
     * 获得zset指定元素的所有集合
     * @param key
     * @return
     */
    Set<String> getZSetValues(String key);

    /**
     * 获得zset指定元素的分数
     * @param key
     * @param zSetValue
     * @return
     */
    Double getZSetScore(String key,String zSetValue);

    /**
     * 获取zset指定元素长度
     * @param key
     * @return
     */
    Long getZSetSize(String key);

    /**
     * 获得zset指定元素的得分范围内起始和结束的范围所有集合
     * @param key
     * @param minScore
     * @param maxScore
     * @param start
     * @param end
     * @return
     */
    Set<String> getZSetValues(String key,Double minScore,Double maxScore,Integer start,Integer end);

    /**
     * 删除zset指定值元素
     * @param key
     * @param value
     */
    Long zSetRemoveByValue(String key,Object value);

    /**
     * 删除zset指定分数范围内元素
     * @param key
     * @param minScore
     * @param maxScore
     */
    Long zSetRemoveByScore(String key,Double minScore,Double maxScore);

    /**
     * 删除zset指定范围内元素
     * @param key
     * @param start
     * @param end
     */
    Long zSetRemoveByRang(String key,Long start,Long end);

    /**
     * 获取key过期时间
     * @param templateTypeKey
     * @return
     */
     Long getTemplateTypeTime(String templateTypeKey) ;

    /**
     * redisTemplatekey续期
     * @param templateTypeKey
     * @param timeout
     * @param timeUnit
     */
    void reSetTemplateTypeExpireTime(String templateTypeKey,Long timeout,TimeUnit timeUnit);
}
