package yun.ling.pm.mapper;

import yun.ling.pm.entity.JianmoModelCountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模型数据统计 Mapper 接口
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
public interface JianmoModelCountMapper extends BaseMapper<JianmoModelCountEntity> {

}
