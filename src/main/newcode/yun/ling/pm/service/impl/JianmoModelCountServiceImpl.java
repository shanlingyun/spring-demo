package yun.ling.pm.service.impl;

import yun.ling.pm.entity.JianmoModelCountEntity;
import yun.ling.pm.mapper.JianmoModelCountMapper;
import yun.ling.pm.service.JianmoModelCountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模型数据统计 服务实现类
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@Service
public class JianmoModelCountServiceImpl extends ServiceImpl<JianmoModelCountMapper, JianmoModelCountEntity> implements JianmoModelCountService {

}
