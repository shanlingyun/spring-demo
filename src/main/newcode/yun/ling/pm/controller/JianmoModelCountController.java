package yun.ling.pm.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 模型数据统计 前端控制器
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@Controller
@RequestMapping("/jianmoModelCountEntity")
public class JianmoModelCountController {

}

