
package yun.ling.pm.controller;

import com.google.gson.Gson;
import io.seata.core.context.RootContext;
import io.seata.core.exception.TransactionException;
import io.seata.core.lock.AbstractLocker;
import io.seata.core.model.BranchStatus;
import io.seata.core.model.BranchType;
import io.seata.core.model.Resource;
import io.seata.rm.DefaultResourceManager;
import io.seata.rm.datasource.DataSourceManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.web.bind.annotation.*;
import yun.ling.pm.dao.SceneSceneMapper;
import yun.ling.pm.dao.UserMapper;
import yun.ling.pm.vo.SceneScene;
import yun.ling.pm.vo.User;

import java.util.List;


@RestController
@Slf4j
@Api(tags = "hello word")
public class HellowController {

    @Autowired
    private SceneSceneMapper sceneSceneMapper;
    @Autowired
    private UserMapper userMapper;

    @RequestMapping(value = "hello" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "hello", tags = "")
    public String hello(){
        List<User> list = userMapper.select();
        return new Gson().toJson(list);
    }

    @RequestMapping(value = "inseruser" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "inseruser", tags = "")
    public String inseruser(@ModelAttribute User user){
        userMapper.insert(user);
        List<User> list = userMapper.select();
        log.error(new Gson().toJson(list));
        RootContext.bindGlobalLockFlag();
        List<User> list2 = userMapper.select();
        log.error(new Gson().toJson(list2));
      //  userMapper.update(user,"sss");
         return new Gson().toJson(list);
    }


    @RequestMapping(value = "updateuser" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "updateuser", tags = "")
    public String updateuser(@ModelAttribute User user){
        userMapper.update(user,"sss");
        List<User> list = userMapper.select();
        log.error(new Gson().toJson(list));
        return new Gson().toJson(list);
    }

    public static class MockDataSourceManager extends DataSourceManager {

        @Override
        public Long branchRegister(BranchType branchType, String resourceId, String clientId, String xid, String applicationData, String lockKeys)
                throws TransactionException {
            throw new RuntimeException("this method should not be called!");
        }

        @Override
        public void branchReport(BranchType branchType, String xid, long branchId, BranchStatus status, String applicationData) throws TransactionException {
            throw new RuntimeException("this method should not be called!");
        }

        @Override
        public boolean lockQuery(BranchType branchType, String resourceId, String xid, String lockKeys)
                throws TransactionException {
            return true;
        }

        @Override
        public void registerResource(Resource resource) {

        }

        @Override
        public void unregisterResource(Resource resource) {

        }

        @Override
        public BranchStatus branchCommit(BranchType branchType, String xid, long branchId, String resourceId, String applicationData) throws TransactionException {
            throw new RuntimeException("this method should not be called!");
        }

        @Override
        public BranchStatus branchRollback(BranchType branchType, String xid, long branchId, String resourceId, String applicationData)
                throws TransactionException {
            throw new RuntimeException("this method should not be called!");
        }
    }
}

