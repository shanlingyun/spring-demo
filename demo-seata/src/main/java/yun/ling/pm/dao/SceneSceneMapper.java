package yun.ling.pm.dao;


import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import yun.ling.pm.vo.SceneScene;

import java.util.List;

@Mapper
public interface SceneSceneMapper {
    @Select("SELECT * FROM t_order0 LIMIT 20")
    List<SceneScene> select();

    @Insert("insert into t_order0(user_id,order_id)values(#{userId},#{orderId})")
    Integer insert(@Param("userId")Long userId, @Param("orderId")Long orderId);
}
