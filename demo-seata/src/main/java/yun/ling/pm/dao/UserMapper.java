package yun.ling.pm.dao;

import org.apache.ibatis.annotations.*;
import org.mybatis.spring.annotation.MapperScan;
import yun.ling.pm.vo.SceneScene;
import yun.ling.pm.vo.User;

import java.util.List;

@Mapper
public interface UserMapper {
    @Select("SELECT * FROM user0 LIMIT 20")
    List<User> select();

    @Insert("insert into user0(id,name,gmt)values(#{user.id},#{user.name},#{user.gmt})")
    Integer insert(@Param("user") User user);

    @Update("UPDATE user0 SET NAME=#{name} WHERE  NAME=#{user.name}")
    Integer update(@Param("user") User user,@Param("name")String name);
}
