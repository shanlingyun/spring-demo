package yun.ling.pm.vo;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;
    private String name;
    private String updateDate;
    private String gmt;
}
