package yun.ling.demo.rabbitmq.service;

 import org.springframework.amqp.core.Message;
 import org.springframework.amqp.core.MessageProperties;
 import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yun.ling.demo.rabbitmq.config.RabbitMqConfig;

@Service
public class RabbitMQService {

    @Autowired
    RabbitTemplate rabbitTemplate;
    public void sendMessage(String message){
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_TEST_D,"",message+":"+RabbitMqConfig.EXCHANGE_TEST_D);
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_TEST_D,"t.r",message+":"+RabbitMqConfig.EXCHANGE_TEST_D);

        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_TEST_T,"t.f",message+":"+RabbitMqConfig.EXCHANGE_TEST_T);
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_TEST_T,"#",message+":"+RabbitMqConfig.EXCHANGE_TEST_T);

        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("k",message);
        messageProperties.setHeader("x-match","all");
        Message message1 = new Message(message.getBytes(),messageProperties);
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_TEST_H,"",message1);

        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_TEST_F,"",message+":"+RabbitMqConfig.EXCHANGE_TEST_F);
    }
}
