package yun.ling.demo.rabbitmq.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import yun.ling.demo.rabbitmq.config.RabbitMqConfig;

@Component
@Slf4j
public class RabbitMQListener {
    @RabbitListener(queues = RabbitMqConfig.QUEUE_TEST_D1)
    public void process1(String content){
       log.error(RabbitMqConfig.QUEUE_TEST_D1+":"+content);
    }
    @RabbitListener(queues = RabbitMqConfig.QUEUE_TEST_D2)
            public void process2(String content){
        log.error(RabbitMqConfig.QUEUE_TEST_D2+":"+content);
    }
    @RabbitListener(queues =RabbitMqConfig.QUEUE_TEST_T1)
            public void process3(String content){
        log.error(RabbitMqConfig.QUEUE_TEST_T1+":"+content);
    }
    @RabbitListener(queues = RabbitMqConfig.QUEUE_TEST_T2)
            public void process4(String content){
        log.error(RabbitMqConfig.QUEUE_TEST_T2+":"+content);
    }
    @RabbitListener(queues = RabbitMqConfig.QUEUE_TEST_F1)
            public void process5(String content){
        log.error(RabbitMqConfig.QUEUE_TEST_F1+":"+content);
    }
    @RabbitListener(queues = RabbitMqConfig.QUEUE_TEST_F2)
    public void process6(String content){
        log.error(RabbitMqConfig.QUEUE_TEST_F2+":"+content);
    }

    @RabbitListener(queues =RabbitMqConfig.QUEUE_TEST_H1)
    public void process7(Message message){
        log.error(RabbitMqConfig.QUEUE_TEST_H1+":"+new String(message.getBody()));
    }
    @RabbitListener(queues = RabbitMqConfig.QUEUE_TEST_H2)
    public void process8(Message message){
        log.error(RabbitMqConfig.QUEUE_TEST_H2+":"+new String(message.getBody()));
    }
}

