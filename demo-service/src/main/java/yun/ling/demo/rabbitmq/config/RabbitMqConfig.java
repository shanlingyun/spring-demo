package yun.ling.demo.rabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {
    @Value("${spring.rabbitmq.port}")
    private Integer port;
    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.password}")
    private String password;
    @Value("${spring.rabbitmq.username}")
    private String username;
    @Value("${spring.rabbitmq.virtual-host}")
    private String virtualHost;

    //exchange类型为direct
    public static final String EXCHANGE_TEST_D ="test";
    public static final String QUEUE_TEST_D1 ="test";
    public static final String QUEUE_TEST_D2 ="test2";

    //exchange类型为fanout
    public static final String EXCHANGE_TEST_F="test_f";
    public static final String QUEUE_TEST_F1="test_f1";
    public static final String QUEUE_TEST_F2="test_f2";

    //exchange类型为headers
    public static final String EXCHANGE_TEST_H="test_h";
    public static final String QUEUE_TEST_H1="test_h1";
    public static final String QUEUE_TEST_H2="test_h2";

    //exchange类型为topic
    public static final String QUEUE_TEST_T1="test_t1";
    public static final String QUEUE_TEST_T2="test_t2";
    public static final String EXCHANGE_TEST_T="test_t";
    @Bean
    public CachingConnectionFactory connectionFactory(){
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host,port);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        cachingConnectionFactory.setVirtualHost(virtualHost);
        return cachingConnectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        return rabbitTemplate;
    }

    @Bean
    public Queue queueTeset(){
        return new Queue(QUEUE_TEST_D1,true);
    }
}
