package yun.ling.demo.rabbitmq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import yun.ling.demo.rabbitmq.service.RabbitMQService;

@RestController
@RequestMapping("rabbit")
public class TestRabbitMQController {
    @Autowired
    private RabbitMQService rabbitMQService;
    @GetMapping("send")
    public String send(String message){
        rabbitMQService.sendMessage(message);
        return "";
    }
}
