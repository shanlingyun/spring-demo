package yun.ling.demo.clone;

import java.io.IOException;

public class DataColon {
    public static void main(String[] args) throws IOException, ClassNotFoundException, CloneNotSupportedException {
        Person person = new Person("20","171",new Car());
        Person person2 = MyUtils.clone(person);
        person2.setAge("30");

         System.out.println(person2.getAge());
        System.out.println(person.getAge());

    }
}
