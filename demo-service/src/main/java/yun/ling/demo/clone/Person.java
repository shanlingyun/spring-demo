package yun.ling.demo.clone;

import java.io.Serializable;

public class Person implements Serializable {
    private String age;
    private String height;
    private Car car;

    public Person(String age, String height, Car car) {
        this.age = age;
        this.height = height;
        this.car = car;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
