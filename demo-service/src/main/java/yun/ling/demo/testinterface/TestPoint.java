package yun.ling.demo.testinterface;


import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class TestPoint {

    public void tess(Class classz) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Object object =classz.newInstance();
        Arrays.asList(classz.getDeclaredMethods()).forEach(method1 -> {
            Arrays.asList(method1.getAnnotations()).forEach(annotation->{
                if(annotation.annotationType().getSimpleName().endsWith("Test")){
                    System.out.println("我们都一起摇啊摇");
                    try {
                        method1.invoke(object);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            });
        });
    }

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        TestPoint testPoint = new TestPoint();
        testPoint.tess(TestCase.class);
     }
}
