package yun.ling.demo.seaweedfs;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import net.anumbrella.seaweedfs.core.Connection;
import net.anumbrella.seaweedfs.core.FileSource;
import  net.anumbrella.seaweedfs.core.FileTemplate;
import net.anumbrella.seaweedfs.core.http.StreamResponse;
import org.apache.http.client.cache.HttpCacheStorage;

public class TestSeaWeedFS {


      FileTemplate fileTemplate;

    public TestSeaWeedFS() throws IOException {
        FileSource fileSource = new FileSource();
        // SeaweedFS master server host
        fileSource.setHost("192.168.10.68");
        // SeaweedFS master server port
        fileSource.setPort(8888);
        // Set Connection Timeout
        fileSource.setConnectionTimeout(5000);
        // Startup manager and listens for the change
        fileSource.startup();
        fileTemplate =new FileTemplate(fileSource.getConnection());
    }

    public static void main(String[] args) throws IOException {
       new TestSeaWeedFS().test();
    }

    public void test() throws IOException {
          StreamResponse streamResponse =fileTemplate.getFileStream("fineUpload");
        OutputStream outputStream = streamResponse.getOutputStream();
    }
}
