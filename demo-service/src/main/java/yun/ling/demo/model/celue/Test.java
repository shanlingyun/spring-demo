package yun.ling.demo.model.celue;

public class Test {
    public static void main(String[] args) {
        WinRaR winRaR = new WinRaR(new Efficient());
        winRaR.compression();
        winRaR.setStragy(new Encrypt());
        winRaR.compression();
        winRaR.setStragy(new Rapid());
        winRaR.compression();
    }
}
