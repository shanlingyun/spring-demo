package yun.ling.demo.model.factory;

public class ProxyPhone {
    public  Phone makePhone(String name){
        if(name.equals("miPHone")){
            return  new MiPhone();
        }else {
            return new IPhone();
        }
    }
}
