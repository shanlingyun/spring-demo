package yun.ling.demo.model.daili;

public class Service implements Subject {
    private String servierName;
    public Service(String servierName){
        System.out.println("servierName = " + servierName);
        this.servierName = servierName;
    }

    @Override
    public void vist(String url) {
        response(url);
    }

    private void response(String url){
        System.out.println("servierName = " + servierName+",url = " + url);
    }
}
