package yun.ling.demo.model.guanchazhe;

public class ConcreteObserver implements Observer {
    @Override
    public void update(String msg) {
        System.out.println("ConcreteObserver receive notify msg:" + msg);
    }
}
