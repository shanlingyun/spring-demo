package yun.ling.demo.model.mingling;

public class MyCommand implements Command {
    private Recever recever;

    public MyCommand(Recever recever){
        this.recever = recever;
    }
    @Override
    public void exec(String message) {
        recever.fark(message);
    }
}
