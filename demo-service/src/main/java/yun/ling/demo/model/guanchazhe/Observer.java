package yun.ling.demo.model.guanchazhe;

public interface Observer {
     void update(String msg);
}
