package yun.ling.demo.model.guanchazhe;

import java.util.ArrayList;
import java.util.List;

public class ConcreteSubject implements Subject {
    int state;
    private List<Observer> observerList = new ArrayList<Observer>();


    @Override
    public void setState(int state) {
        this.state = state;
        notify("new state:"+state);
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public void attach(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notify(String msg) {
        observerList.forEach( observer ->observer.update(msg));
    }
}
