package yun.ling.demo.model.guanchazhe;

public interface Subject {
    public void setState(int state);
    public int getState();
    public void attach(Observer observer);
    public void detach(Observer observer);
    public void notify(String msg);
}
