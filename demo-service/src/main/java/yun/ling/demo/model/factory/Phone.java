package yun.ling.demo.model.factory;

public interface Phone {
    void make();
}
