package yun.ling.demo.model.mingling;

import io.swagger.models.auth.In;

/**
 * 司令员
 */
public class Invoker {
    Command command;
    public Invoker(Command command){
        this.command = command;
    }
    public void exec(String message){
        command.exec(message);
    }
}
