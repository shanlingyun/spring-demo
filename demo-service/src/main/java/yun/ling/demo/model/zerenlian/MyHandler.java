package yun.ling.demo.model.zerenlian;

public class MyHandler extends AbstractHandler implements Handler{
    String name;
    public MyHandler(String name){
        this.name= name;
    }
    @Override
    public void opera() {
        System.out.println("haha:"+name);
        if(getHandler()!=null) {
            getHandler().opera();
        }
    }
}
