package yun.ling.demo.model.factory;

public class MiPhone implements Phone {
    public MiPhone(){
        this.make();
    }
    @Override
    public void make() {
        System.out.println("i am miphone");
    }
}
