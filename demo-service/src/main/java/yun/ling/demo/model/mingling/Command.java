package yun.ling.demo.model.mingling;

/**
 * 命令
 */
public interface Command {
    void exec(String message);
}
