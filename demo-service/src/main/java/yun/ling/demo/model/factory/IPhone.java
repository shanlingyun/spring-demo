package yun.ling.demo.model.factory;

public class IPhone implements Phone {

    public IPhone(){
        this.make();
    }

    @Override
    public void make() {
        System.out.println("i am iphone");
    }
}
