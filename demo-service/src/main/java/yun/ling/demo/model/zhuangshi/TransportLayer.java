package yun.ling.demo.model.zhuangshi;

import javax.xml.crypto.Data;

public abstract class  TransportLayer implements Datagram {

    protected Datagram datagram;

    public  TransportLayer(Datagram datagram){
        this.datagram = datagram;
    }

    public void send(){
        datagram.send();
    }

}
