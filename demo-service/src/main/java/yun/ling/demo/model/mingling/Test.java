package yun.ling.demo.model.mingling;

public class Test {


    public static void main(String[] args) {
        //命令执行者
        Recever recever = new Recever();
        //命令传输
        Command command = new MyCommand(recever);
        //命令发起者
        Invoker invoker = new Invoker(command);
        invoker.exec("hahah,nihao");
    }
}
