package yun.ling.demo.model.zhuangshi;

import javax.xml.crypto.Data;

public class Test {
    public static void main(String[] args) {
        Datagram datagram = new AppDatagram();
        TransportLayer useTCP = new UseTCP(datagram);
        useTCP.send();
        TransportLayer useUDP = new UseUDP(datagram);
        useUDP.send();
    }
}
