package yun.ling.demo.model.factory;

public class Test {
    public static void main(String[] args) {
        ProxyPhone phoneFactory = new ProxyPhone();
        Phone miPhone =phoneFactory.makePhone("miPHone");
        Phone iphone = phoneFactory.makePhone("iphone");

    }
}
