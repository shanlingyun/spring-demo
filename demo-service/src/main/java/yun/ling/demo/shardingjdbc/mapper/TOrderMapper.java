package yun.ling.demo.shardingjdbc.mapper;


import yun.ling.demo.shardingjdbc.model.TOrder;
import yun.ling.demo.shardingjdbc.base.BasicMapper;

public interface TOrderMapper extends BasicMapper<TOrder> {
}