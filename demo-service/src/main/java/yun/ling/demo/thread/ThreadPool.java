package yun.ling.demo.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class ThreadPool {
    //创建一个指定线程数线程池
     ExecutorService executorService = Executors.newFixedThreadPool(10);
     //创建当个线程给线程池，如果当前线程异常终结，会单独创建一个新的线程，可以保证队列中的程序是按照顺序执行
     ExecutorService executorService2 = Executors.newSingleThreadExecutor();
     //创建一个可缓存的线程池，如果线程数超过了需求，则自动回收部分线程，如果线程数不够，则自动创建一些线程
     ExecutorService executorService3 = Executors.newCachedThreadPool();

    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool();
        threadPool.testExecutorService();
        threadPool.testExecutorService2();
        threadPool.testExecutorService3();
    }

     public void testExecutorService(){
         executorService.execute(new Runnable(){
           @Override
           public void run(){
               System.out.println("executorService");
           }
         });
         //扩展callable 和 FutureTask
         executorService.execute(new FutureTask(new Callable() {
             @Override
             public Object call() throws Exception {
                 return null;
             }
         }));
     }

    public void testExecutorService2(){
        executorService2.execute(new Runnable(){
            @Override
            public void run(){
                System.out.println("executorService2");
            }
        });
    }

    public void testExecutorService3(){
        executorService3.execute(new Runnable(){
            @Override
            public void run(){
                System.out.println("executorService3");
            }
        });
    }

}
