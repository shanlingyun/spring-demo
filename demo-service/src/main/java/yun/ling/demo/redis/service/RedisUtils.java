package yun.ling.demo.redis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisUtils {
    @Autowired
    private RedisTemplate redisTemplate;

    public void addRedis(String key,String value){
        redisTemplate.opsForValue().setIfAbsent(key,value);
    }

    public void addTimeRedis(String key,String value,long time){
        redisTemplate.opsForValue().set(key,value,time, TimeUnit.SECONDS);
    }

    public Object getRedis(String key){
        return redisTemplate.opsForValue().get(key);
    }
}
