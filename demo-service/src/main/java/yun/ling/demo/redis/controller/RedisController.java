package yun.ling.demo.redis.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import yun.ling.demo.redis.service.RedisUtils;

@RestController("")
@Slf4j
public class RedisController {

    @Autowired
    private RedisUtils redisUtils;

    @GetMapping("redis2/test2")
    public String addRedis(String key,String value,long time){
        redisUtils.addTimeRedis(key,value,time);
        return (String)redisUtils.getRedis(key);
    }
}
