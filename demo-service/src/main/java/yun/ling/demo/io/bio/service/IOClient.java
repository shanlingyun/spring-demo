package yun.ling.demo.io.bio.service;


import java.io.IOException;
import java.net.Socket;
import java.util.Date;

public class IOClient {
    public static void main(String[] args) {
            try {
                Socket socket = new Socket("localhost",3333);
                new Thread(()->{
                    while (true){
                        try {
                            socket.getOutputStream().write((new Date()+"").getBytes());
                            Thread.sleep(5000);
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

}
