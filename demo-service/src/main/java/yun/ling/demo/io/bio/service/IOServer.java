package yun.ling.demo.io.bio.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;

public class IOServer {
    public static void main(String[] args) {
        new Thread(() ->{

            try {
                ServerSocket  serverSocket = new ServerSocket(1938);
                Socket socket =serverSocket.accept();
                new Thread(() -> {
                    try {
                        int len;
                        byte[] data = new byte[1024];
                        InputStream inputStream = socket.getInputStream();
                        // 按字节流方式读取数据
                        while ((len = inputStream.read(data)) != -1) {
                            System.out.println(new String(data, 0, len));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }).start();
        } catch (IOException e) {
                e.printStackTrace();
            }
    }).start();
    }

}
