# demo

java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-extension-server/demo-nacos/demo-nacos-consumer/target/demo-nacos-consumer-1.0-SNAPSHOT.jar
java -Dfile.encoding=utf-8 -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-extension-server/demo-nacos/demo-nacos-server/target/demo-nacos-server-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-extension-server/demo-zk/demo-zk-server/target/demo-zk-server-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-extension-server/demo-zk/demo-zk-consumer/target/demo-zk-consumer-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-admin/target/demo-admin-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/Sentinel/sentinel-dashboard/target/sentinel-dashboard.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-hystrix/target/demo-hystrix-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-getway/target/demo-getway-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-extension-server/demo-consul/demo-consul-server/target/demo-consul-server-1.0-SNAPSHOT.jar
java -jar -Xms128m -Xmx512m D:/CODING_SOURCE/open_source/spring-demo/demo-extension-server/demo-consul/demo-consul-client/target/demo-consul-client-1.0-SNAPSHOT.jar

#### 介绍

1.  demo-admin(spring boot服务治理管理admin，端口号：8099，访问地址：127.0.0.1:8099) 
2.  demo-consul-client(spring boot服务消费者，端口号：8801)
3.  demo-consul-server(spring boot服务服务端，端口号：8800)
4.  demo-seata(spring boot分布式事务服务，端口号：8200)
5.  demo-hystrix(spring boot微服务熔断服务，端口号：9001，访问地址：http://127.0.0.1:9001/hystrix)
6.  demo-eureka(eureka注册中心，端口号：8761，访问地址：http://127.0.0.1:8761)
7.  demo-zk-server(zk注册中心提供服务，端口号：8870，访问地址：http://127.0.0.1:8870/hello?name=sly)
8.  demo-zk-consumer(zk注册中心提供客户端，端口号：8875，访问地址：http://127.0.0.1:8875/consumer?name=sly)
9.  demo-getway(getway网关服务，端口号：8875，访问地址：http://127.0.0.1:8875/consumer?name=sly)
10.  demo-nacos-server(zk注册中心提供服务，端口号：8880，访问地址：http://127.0.0.1:8880/hello?name=sly)
11.  demo-nacos-consumer(zk注册中心提供客户端，端口号：8885，访问地址：http://127.0.0.1:8885/consumer?name=sly)

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

### 1.  接入sentinel方式：
  - a.)、pom文件导入依赖gav： 
```xml
  <dependency>
      <groupId>com.alibaba.cloud</groupId>
      <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
      <version>${spring.cloud.version}</version>
  </dependency>
```
  - b.)、yml资源文件导入： 
```yaml
spring:
  cloud:
    sentinel:
      transport:
    # 默认8619端口，如果被占用自动从8619 开始 +1 一直找到未被占用的端口为止
        port: 8619
    # 配置sentinel dashboard 地址
        dashboard: 127.0.0.1:8999
```   
### 2.  接入springboot admin 
  - a.)、yml资源文件导入：
```yaml
management:
endpoints:
  web:
    exposure:
      include: "*"
spring:
 cloud:
  nacos:
    discovery:
      server-addr: 127.0.0.1:8848
      service: ${spring.application.name}
      namespace: f9cd2107-5496-46ce-b003-3cc9b41849aa
```
  - b)、pom引入gav
```xml
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
            <version>${spring.cloud.version}</version>
        </dependency>
```
  - c)、启动类添加注解
```java
@SpringCloudApplication
或者
@SpringBootApplication
@EnableDiscoveryClient
```
#### 3.  接入ko-time
参考路径：http://kotimedoc.langpy.cn/#/v204/getstart?id=%e5%bc%95%e5%85%a5%e4%be%9d%e8%b5%96
```xml
  <dependency>
     <groupId>cn.langpy</groupId>
     <artifactId>ko-time</artifactId>
     <version>2.1.1</version>
   </dependency>
```
```yaml
ko-time:
  log-language: chinese # 控制台输出语言（english/chinese）非必填，默认chinese
  log-enable: true  #
  threshold: 800.0 # 时间阈值，用于前端展示，大于阈值显示红色，小于阈值显示绿色，非必填，默认800
  pointcut: execution(* yun.ling.pm.controller.*.*(..))
  auth-enable: true
  user-name: shanly
  password: 123456
```
### 4、生产smart-doc在线文档：
 
####  4-1) 、pom.xml向build标签下的plugs追加加配置如下
  
```xml
    <plugin>
        <groupId>com.github.shalousun</groupId>
        <artifactId>smart-doc-maven-plugin</artifactId>
        <version>2.2.8</version>
        <configuration>
            <configFile>./src/main/resources/smart-doc.json</configFile>
        </configuration>
        <executions>
            <execution>
                <goals>
                    <goal>html</goal>
                    <goal>postman</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
```
####  4-2) 、定义smart-doc.json
``` json5
{
  "projectName": "51建模接口在线文档",
  "serverUrl": "http://192.168.20.85:8090",
  "isStrict": false,
  "allInOne": true,
  "coverOld": true,
  "style": "xt256",
  "inlineEnum": true,
  "displayActualType": true,
  "packageFilters": "com.jmyd.jianmo.api.combo.controller.*,com.jmyd.jianmo.admin.combo.controller.*",
  "debugEnvName" : "开发环境",
  "language": "CHINESE",
  "outPath": "自定义文档输出目录",
  "apiConstants": [{
    "constantsClassName": "com.jmyd.jianmo.admin.common.constant.FilterProFixConstant"
  }],
  "errorCodeDictionaries": [
    {
      "title": "系统错误码",
      "enumClassName": "com.jmyd.jianmo.utils.result.ResponseConstant",
      "codeField": "code",
      "descField": "errorMsg"
    }
  ]
}
```
编译打包：mvn -Dfile.encoding=UTF-8 smart-doc:html
补充：查看切面是否配置成功(auth-enable: false)，http://localhost:8881/koTime/getConfig

#### 参与贡献

1.  Fork 本仓库


#### 特技

1.  
