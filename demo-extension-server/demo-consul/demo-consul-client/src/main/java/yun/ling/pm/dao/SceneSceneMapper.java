package yun.ling.pm.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import yun.ling.pm.vo.SceneScene;

import java.util.List;

@Mapper
public interface SceneSceneMapper {
    @Select("SELECT ID,VERSION FROM SCENE_SCENE LIMIT 20")
    List<SceneScene> select();
}
