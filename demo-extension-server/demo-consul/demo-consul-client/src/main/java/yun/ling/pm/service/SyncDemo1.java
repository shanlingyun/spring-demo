package yun.ling.pm.service;

import org.springframework.stereotype.Service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class SyncDemo1 {
   Lock lock = new ReentrantLock();

            int  i=0;
    public         void  test( ) {
        lock.lock();
        try {
            Thread.sleep(1000);
            i++;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("threadName = ,i="+i);
        i++;
        System.out.println(Thread.currentThread().getName());
        lock.unlock();
    }
}
