package yun.ling.pm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@ConditionalOnProperty(prefix = "swagger",value = {"enable"},havingValue = "true")
@ComponentScan
public class SwaggerConfig extends WebMvcConfigurationSupport {
    @Value("${testserver.swagger.title}")
    private String title;
    @Value("${testserver.swagger.description}")
    private String  description;
    @Value("${testserver.swagger.version}")
    private String version;
    @Value("${testserver.swagger.termsOfServiceUrl}")
    private String termsOfServiceUrl;
    @Value("${testserver.swagger.contact}")
    private String contact;
    @Value("${testserver.swagger.license}")
    private String license;
    @Value("${testserver.swagger.licenseUrl}")
    private String licenseUrl;
    @Bean
    public Docket commonDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("spring-test")
                        .contact(new Contact(this.contact,"",""))
                        .version("v2")
                        .build())
                        .select().apis(RequestHandlerSelectors.basePackage("yun.ling.shan.controller")).build();
    }
}
