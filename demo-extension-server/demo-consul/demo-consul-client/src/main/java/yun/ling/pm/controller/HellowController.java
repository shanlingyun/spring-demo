package yun.ling.pm.controller;

import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import yun.ling.pm.dao.SceneSceneMapper;
import yun.ling.pm.service.SyncDemo1;

@RestController
@Slf4j
@Api(tags = "hello word")
public class HellowController {
    @Autowired
    private SceneSceneMapper sceneSceneMapper;


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SyncDemo1 demo1;

    @RequestMapping(value = "conmuser" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "hello", tags = "")
    public String hello(){
        Gson gson = new Gson();
        String result =  gson.toJson(restTemplate.getForObject("http://demo-consul-server/hello",String.class));
        log.info(result);
        return result;
    }

    @RequestMapping(value = "test" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "test", tags = "")
    public String test(){
        demo1.test();
        return"";
    }
}