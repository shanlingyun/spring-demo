
package yun.ling.pm.service;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class ObjInfoUtils {
    //json文件解析地址ExecutorService
    private static final String TEMPLATE_URL="D:\\shanly\\nginx-1.18.0\\html_bac\\scene-portal\\4650082d-5cec-9f1a-e933-3ba052adf0d5_old";

    public static void main(String[] args) throws IOException {
        new ObjInfoUtils().updateFile(TEMPLATE_URL);
    }


    /**
     * 文件处理
     * @param url
     */
    public void updateFile(String url)   {
        File file =new File(url);
        String nameList[]=file.list();
        try {
            if(file.getPath().indexOf("api")!=-1 ||file.getPath().indexOf("viewInfo")!=-1){
                //json数据处理
                jsonDataFile(file);
                return ;
            }

            if (nameList == null || nameList.length <= 0) {
                return;
            }
            for (String fileName : nameList) {
                if (file.isDirectory()) {
                    url = file.getPath() + "\\" + fileName;
                    updateFile(url);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void jsonDataFile( File file){
        String fileStr =FileUtils.fileReader(file);
        Map<String,Object> map = new Gson().fromJson(fileStr, HashMap.class);
        //导出图片
        //imageFileDown(map);
        // 导出mp3音乐
        //modelmp3FileDown(map);
        //文件写出
        for(String replaceHost:GlobPath.REPLACE_DATA){
            fileStr = fileStr.replaceAll(replaceHost, GlobPath.MODEL_LOCAL_HOST);
        }

        log.info("文件地址如下：" + file.getPath());
        FileUtils.fileWriter(fileStr,file);
    }
    /**
     * 导出图片
     * @param map
     */
    public void imageFileDown( Map<String,Object> map){
        if(map.containsKey("data")){
            return;
        }
        Object object = map.get("data");
        LinkedTreeMap<String,Object> map2 = (LinkedTreeMap)object;
        if(map2.containsKey("thumbnail")&& !StringUtils.isEmpty(map2.get("thumbnail"))){
            String thumbnail = map2.get("thumbnail").toString();
            log.info("图片文件下载，下载thumbnail:"+thumbnail);
            FileUtils.netSourceDown(thumbnail,GlobPath.IMAGE_REPLACE,GlobPath.IMG_FILE_OUT);
        }
    }

    /**
     * 导出mp3音乐
     * @param map
     */
    public void modelmp3FileDown( Map<String,Object> map){
        if(!map.containsKey("data")){
            return;
        }
        Object object = map.get("data");
        LinkedTreeMap<String,Object> map2 = (LinkedTreeMap)object;
        if(!map2.containsKey("extData") || StringUtils.isEmpty(map2.get("extData"))){
            return ;
        }
        LinkedTreeMap<String,Object> extData = (LinkedTreeMap)map2.get("extData");
        LinkedTreeMap<String,Object> info = (LinkedTreeMap)extData.get("info");
      String  custommap =  info.get("custom").toString();
        Map<String,Object> datamap = new Gson().fromJson(custommap, HashMap.class);
        LinkedTreeMap<String,Object> detail_audio = (LinkedTreeMap)datamap.get("detail_audio");
        if(detail_audio==null ||detail_audio.get("musicFile")==null){
            return ;
        }
       String music = (String)detail_audio.get("musicFile");
        FileUtils.netSourceDown(music, GlobPath.IMAGE_MODEL_REPLACE, GlobPath.MP3_MODEL_FILE_OUT);

        log.info(custommap);
    }


    public void mp3FileDown( Map<String,Object> map){
        if(!map.containsKey("data")){
            return;
        }
        Object object = map.get("data");
        LinkedTreeMap<String,Object> map2 = (LinkedTreeMap)object;
        if(!map2.containsKey("musicUrl") || StringUtils.isEmpty(map2.get("musicUrl"))){
            return ;
        }
        String musicUrl = map2.get("musicUrl").toString();
        if(musicUrl.indexOf(GlobPath.MUSIC_REPLACE)!=-1) {
            log.info("51jianmo需要下载musicUrl:"+musicUrl);
            FileUtils.netSourceDown(musicUrl, GlobPath.MUSIC_REPLACE, GlobPath.MP3_FILE_OUT);
        }
        if(musicUrl.indexOf(GlobPath.MUSIC_JIMU_REPLACE)!=-1) {
            log.info("jimuyida需要下载musicUrl:"+musicUrl);
            FileUtils.netSourceDown(musicUrl, GlobPath.MUSIC_JIMU_REPLACE, GlobPath.MP3_JIMU_FILE_OUT);
        }
    }
}
