package yun.ling.pm.service;

public class GlobPath {
    public static final String REPLACE_DATA[]={"https://obs.3dyunzhan.com","https://www.3dyunzhan.com","https://oss.51jianmo.com", "https://file.jimuyida.com"};
    public static  final String MODEL_LOCAL_HOST="http://127.0.0.1";

    //图片文件地址替换
    public static  final String IMAGE_REPLACE="https://obs.3dyunzhan.com/pictures/";
    //mp3址替换
    public static  final String MUSIC_REPLACE="http://oss.51jianmo.com/scene/extObj/music/";
    //积木mp3址替换
    public static  final String MUSIC_JIMU_REPLACE="http://file.jimuyida.com/scene/extObj/music/";

    public static  final String MUSIC_URLS[]={"http://file.jimuyida.com/scene/extObj/music/",
            "https://file.jimuyida.com/scene/extObj/music/",
            "https://obs.3dyunzhan.com/scene/extObj/music/",
            "http://obs.3dyunzhan.com/scene/extObj/music/",
            "http://oss.51jianmo.com/scene/extObj/music/",
            "https://oss.51jianmo.com/scene/extObj/music/"};



    /*//图片文件输出地址
    public static final String IMG_FILE_OUT="D:\\shanly\\nginx-1.18.0\\html\\pictures\\";
    //mp3地址下载
    public static final String MP3_FILE_OUT="D:\\shanly\\nginx-1.18.0\\html\\scene\\extObj\\music\\";
    //积木mp3地址下载
    public static final String MP3_JIMU_FILE_OUT="D:\\shanly\\nginx-1.18.0\\html\\scene\\extObj\\music\\";*/

    //图片文件输出地址
    public static final String IMG_FILE_OUT="D:\\nginx-1.18.0\\html\\zmt\\pictures\\";
    //mp3地址下载
    public static final String MP3_FILE_OUT="D:\\nginx-1.18.0\\html\\zmt\\scene\\extObj\\music\\";
    //积木mp3地址下载
    public static final String MP3_JIMU_FILE_OUT="D:\\nginx-1.18.0\\html\\zmt\\scene\\extObj\\music\\";

    public static final String MP3_MODEL_FILE_OUT="D:\\shanly\\nginx-1.18.0\\html\\musics";
    public static  final String IMAGE_MODEL_REPLACE="https://obs.3dyunzhan.com/musics";

}
