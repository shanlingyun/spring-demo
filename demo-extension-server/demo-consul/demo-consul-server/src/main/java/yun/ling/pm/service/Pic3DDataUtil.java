
package yun.ling.pm.service;

import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class Pic3DDataUtil {
    private static final String TEMPLATE_URL="D:\\shanly\\nginx-1.18.0\\html\\scene-portal\\4650082d-5cec-9f1a-e933-3ba052adf0d5\\api_old";

    public static void main(String[] args) throws IOException {
        new Pic3DDataUtil().updateFile(TEMPLATE_URL);
    }


    /**
     * 文件处理
     * @param url
     */
    public void updateFile(String url)   {
        File file =new File(url);
        String nameList[]=file.list();
        try {
            if(file.getPath().indexOf("api_old\\info")!=-1){
                //json数据处理
                pic3DData(file);
                return ;
            }
            if (nameList == null || nameList.length <= 0) {
                return;
            }
            for (String fileName : nameList) {
                if (file.isDirectory()) {
                    url = file.getPath() + "\\" + fileName;
                    updateFile(url);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 导出mp3音乐
     * @param map
     */
    public void pic3DData(File file){
        String fileStr =FileUtils.fileReader(file);
        Map<String,Object> map = new GsonBuilder().disableHtmlEscaping().create().fromJson(fileStr, HashMap.class);
        if(!map.containsKey("data")){
            FileUtils.fileWriter(new GsonBuilder().disableHtmlEscaping().create().toJson(map),file);
            return;
        }
        Object object = map.get("data");
        LinkedTreeMap<String,Object> map2 = (LinkedTreeMap)object;

        if(!map2.containsKey("extobjs")){
            FileUtils.fileWriter(new GsonBuilder().disableHtmlEscaping().create().toJson(map),file);
            return ;
        }
        List<LinkedTreeMap<String,Object>> list= (List<LinkedTreeMap<String,Object>>)map2.get("extobjs");
        if(list.isEmpty()){
            FileUtils.fileWriter(new GsonBuilder().disableHtmlEscaping().create().toJson(map),file);
            return ;
        }

        List<LinkedTreeMap<String,Object>> newList =new ArrayList<LinkedTreeMap<String,Object>>();
        for(LinkedTreeMap<String,Object> linkmap:list){
            String listMusic =(String)linkmap.get("music");
            //listmusic文件下载
            listMusic =musicDown(listMusic);
            if(!StringUtils.isEmpty(listMusic)) {
                linkmap.put("music", listMusic);
            }
            LinkedTreeMap<String,Object> extdataMap = ( LinkedTreeMap<String,Object>)linkmap.get("extdata");
            if(extdataMap.isEmpty()){
                newList.add(linkmap);
                continue;
            }
            LinkedTreeMap<String,Object> infoMap = ( LinkedTreeMap<String,Object>)extdataMap.get("info");
            if(infoMap.isEmpty()){
                newList.add(linkmap);
                continue;
            }
            String infomusic=  (String)infoMap.get("music");
            //infomusic文件下载
            infomusic =musicDown(infomusic);
            if(!StringUtils.isEmpty(infomusic)) {
                infoMap.put("music", infomusic);
                extdataMap.put("info", infoMap);
                linkmap.put("extdata", extdataMap);
            }

            String url = (String) infoMap.get("url");
            if(StringUtils.isEmpty(url)){
                newList.add(linkmap);
                continue;
            }
            String code ="";
            if(url.indexOf("https://www.51jianmo.com/model")!=-1){
                code = url.substring("https://www.51jianmo.com/model?".length());
            }else if(url.indexOf("http://www.51jianmo.com/model")!=-1){
                code = url.substring("http://www.51jianmo.com/model?".length());
            }else{
                code = url.substring("//www.51jianmo.com/model?".length());
            }
            if (StringUtils.isEmpty(code)){
                newList.add(linkmap);
                continue;
            }
            String paramCode[] = code.split("&");
            if (paramCode.length<=0){
                newList.add(linkmap);
                continue;
            }
            if(paramCode[0].contains("code=P")) {
                String  code2 =paramCode[0].replaceAll("code=","");
                infoMap.put("url","/pic3d/"+code2+".html");
                log.info(infoMap.get("url").toString());
            }
            if(paramCode[0].contains("code=M")) {
                String  code2 =paramCode[0].replaceAll("code=","");
                infoMap.put("url","/model/"+code2+".html");
                log.info(infoMap.get("url").toString());

            }
            extdataMap.put("info",infoMap);
            linkmap.put("extdata",extdataMap);
            newList.add(linkmap);
        }
        map2.put("extobjs",newList);
        map.put("data",map2);
        fileStr = fileStr.replaceAll("https", "http");
        //文件写出
        for(String replaceHost:GlobPath.REPLACE_DATA){
            fileStr = fileStr.replaceAll(replaceHost, GlobPath.MODEL_LOCAL_HOST);
        }
        FileUtils.fileWriter(new GsonBuilder().disableHtmlEscaping().create().toJson(map),file);
    }

    private  String musicDown(String musicUrl){
        if(StringUtils.isEmpty(musicUrl)){
            return musicUrl;
        }
        for(String sourceUrl:GlobPath.MUSIC_URLS){
            if(musicUrl.indexOf(sourceUrl)!=-1) {
                 FileUtils.netSourceDown(musicUrl, sourceUrl, GlobPath.MP3_FILE_OUT);
                musicUrl = musicUrl.replaceAll(sourceUrl,"/scene/extObj/music/");
                log.info("51jianmo需要下载musicUrl:"+musicUrl);
            }
        }
        return  musicUrl;
    }
}
