
package yun.ling.test;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import  org.junit.Test;
import yun.ling.pm.dao.SceneSceneMapper;
import yun.ling.pm.vo.SceneScene;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestProgram.class)
@EnableAutoConfiguration(exclude ={ DataSourceAutoConfiguration.class})
@Slf4j
public class TestProgram {

    @Autowired
    private SceneSceneMapper sceneSceneMapper;

    @Test
    public void testHellow(){
        List<SceneScene> sceneSceneList =sceneSceneMapper.select();
        log.info(new Gson().toJson(sceneSceneList));
    }

}

