package yun.ling.pm.config;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class RunTimeHandler   {
    @Pointcut("execution(* yun.ling.pm..*(..))")
    public void prog() {

    }
}
