package yun.ling.pm.shardingjdbc.mapper;


import yun.ling.pm.shardingjdbc.base.BasicMapper;
import yun.ling.pm.shardingjdbc.model.TOrder;

public interface TOrderMapper extends BasicMapper<TOrder> {
}
