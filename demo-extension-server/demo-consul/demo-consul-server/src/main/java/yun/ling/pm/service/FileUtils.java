package yun.ling.pm.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class FileUtils {

    //初始化线程池个数为20
    private static ExecutorService executorThreadPool = Executors.newFixedThreadPool(5);
    /**
     * 网络文件下载
     * @param sourceUrl
     * @param replace
     * @param fileOut
     */
    /*public static void netSourceDown(final String sourceUrl,String replace,String fileOut){
        executorThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                URL url =null;
                HttpURLConnection urlConnection  =null;
                InputStream inputStream = null;
                FileOutputStream fileOutputStream=null;
                File imageFileOut = null;
                try {
                    url = new URL(sourceUrl);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    inputStream = urlConnection.getInputStream();
                    String outputimageFile = fileOut+sourceUrl.replaceAll(replace,"");
                    log.info("资源文件下载地址："+outputimageFile);
                    log.info("sourceUrl"+sourceUrl);
                    imageFileOut = new File(outputimageFile);
                    fileOutputStream = new FileOutputStream(imageFileOut);
                    outputimageFile.getBytes();
                    byte byt[]=new byte[1024];
                    int n=0;
                    while ((n=inputStream.read(byt))>0){
                        fileOutputStream.write(byt,0,n);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }*/

    public static void netSourceDown(final String sourceUrl,String replace,String fileOut){
        executorThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                URL url =null;
                HttpURLConnection urlConnection  =null;
                InputStream inputStream = null;
                FileOutputStream fileOutputStream=null;
                File imageFileOut = null;
                try {
                    url = new URL(sourceUrl);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    inputStream = urlConnection.getInputStream();
                    String outputimageFile = fileOut+sourceUrl.replaceAll(replace,"");
                    log.info("资源文件下载地址："+outputimageFile);
                    log.info("sourceUrl"+sourceUrl);

                    fileOutputStream = new FileOutputStream(new File(outputimageFile));
                    ByteArrayOutputStream output = new ByteArrayOutputStream();

                    byte[] buffer = new byte[1024];
                    int length;

                    while ((length = inputStream.read(buffer)) > 0) {
                        output.write(buffer, 0, length);
                    }
                    byte[] context=output.toByteArray();
                    fileOutputStream.write(output.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    private static void downloadPicture(String urlList) {
        URL url = null;
        int imageNumber = 0;

        try {
            url = new URL(urlList);
            DataInputStream dataInputStream = new DataInputStream(url.openStream());

            String imageName =  "E:/test.jpg";

            FileOutputStream fileOutputStream = new FileOutputStream(new File(imageName));
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int length;

            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            byte[] context=output.toByteArray();
            fileOutputStream.write(output.toByteArray());
            dataInputStream.close();
            fileOutputStream.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //读入文件
    public static String fileReader(File fileout){
        FileReader fileReader =null;
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            fileReader = new FileReader(fileout);
            bufferedReader = new BufferedReader(fileReader);
            String s = "";
            while ((s = bufferedReader.readLine()) != null) {
                stringBuffer.append(s).append("\r\n");
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
        }
        return stringBuffer.toString();
    }

    //写出文件
    public static void fileWriter(String str,File file){
        File parentfileout =new File(file.getParent().replaceAll("shanly", "yun/ling/test"));
        if(!parentfileout.exists()){
            parentfileout.mkdirs();
        }
        File fileout =new File(file.getPath().replaceAll("_old",""));
        log.info("文件修改替换后保存地址："+fileout.getPath());

        Writer writer= null;
        try {
            writer= new OutputStreamWriter(new FileOutputStream(fileout),"UTF-8");
            writer.write(str);
            writer.flush();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}