package yun.ling.pm.service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
@Slf4j
public class TemplateUtil {
    //json文件解析地址ExecutorService
     private static final String TEMPLATE_URL="D:\\shanly\\nginx-1.18.0\\html\\scene-portal\\view\\template_old";

    public static void main(String[] args) throws IOException {
        new TemplateUtil().updateFile(TEMPLATE_URL);
    }


    /**
     * 文件处理
     * @param url
     */
    public void updateFile(String url)   {
        File file =new File(url);
        String nameList[]=file.list();
        try {
            if(file.getPath().indexOf(".html")>-1){
                //模板文件修改
                templateHtml(file);
                return ;
            }

            if (nameList == null || nameList.length <= 0) {
                return;
            }
            for (String fileName : nameList) {
                if (file.isDirectory()) {
                    url = file.getPath() + "\\" + fileName;
                    updateFile(url);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 模板文件修改
     * @param file
     */
    private void templateHtml( File file){
        String fileStr =FileUtils.fileReader(file);

        log.info("模板文件修改修改下载："+file.getPath());
        for(String replaceHost:GlobPath.REPLACE_DATA){
            fileStr = fileStr.replaceAll("https:\\/\\/"+replaceHost, "");
            fileStr = fileStr.replaceAll("http:\\/\\/"+replaceHost, "");
        }
        FileUtils.fileWriter(fileStr,file);
        log.info(fileStr);

    }

}
