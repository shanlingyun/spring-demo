package yun.ling.pm.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author shanlingyun
 * @since 2021-02-20
 */
@Controller
@RequestMapping("/tOrder0Entity")
public class TOrder0Controller {

}

