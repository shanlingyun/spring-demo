
package yun.ling.pm.controller;

import com.google.gson.Gson;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yun.ling.pm.shardingjdbc.model.TOrder;
import yun.ling.pm.shardingjdbc.service.TestService;

import java.util.List;

@RestController
@Slf4j
@Api(tags = "hello word")
public class HellowController {

    @Autowired
    private TestService testService;


    @RequestMapping(value = "hello" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "hello", tags = "")
    public String hello(@ModelAttribute TOrder tOrder){
        List<TOrder> list=testService.selectAll(tOrder);
        Gson gson = new Gson();
       String result =  gson.toJson(list);
       log.info(result);
        return result;
    }

    @HystrixCommand(fallbackMethod = "fallback_insert",commandProperties = {
            //设置这个线程的超时时间是3s，3s内是正常的业务逻辑，超过3s调用fallbackMethod指定的方法进行处理
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")
    })
    @RequestMapping(value = "insert" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "insert", tags = "")
    public String insert(@RequestParam Integer id){
        testService.insert(id);
        return "";
    }

    @RequestMapping(value = "fallback_insert" ,method = {RequestMethod.GET,RequestMethod.POST})
    @ApiOperation(value = "insert", tags = "")
    public String fallback_insert(@RequestParam Integer id){
       log.error("出错熔断");
        return "";
    }
}
