package yun.ling.pm.utils;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * <p>
 * 代码生成器演示
 * </p>
 */
public class DataMapperUtil {
    public static void main(String[] args) {
        new DataMapperUtil().makeTableCode();
    }
   public void makeTableCode(){
       AutoGenerator gen = new AutoGenerator();

//mybatis-plus 全局配置信息
       GlobalConfig globalConfig = new GlobalConfig();
       globalConfig.setAuthor("shanlingyun");//作者名
       String projectPath = System.getProperty("user.dir");
//生成文件的输出目录
       globalConfig.setOutputDir(projectPath + "/src/main/newcode");
//生成后是否打开文件夹
       globalConfig.setOpen(false);
//时间类型对应策略：只使用java.util.date代替
       globalConfig.setDateType(DateType.ONLY_DATE);
       globalConfig.setFileOverride(true);//是否覆盖已有文件
       globalConfig.setBaseResultMap(true);//开启BaseResultMap
       globalConfig.setBaseColumnList(true);//开启baseColumnList
//自定义文件命名，注意:%s会自动填充表实体属性
       globalConfig.setMapperName("%sMapper");
       globalConfig.setXmlName("%sMapper");
       globalConfig.setServiceName("%sService");
       globalConfig.setServiceImplName("%sServiceImpl");
       globalConfig.setControllerName("%sController");
       globalConfig.setEntityName("%sEntity");//实体名字
       gen.setGlobalConfig(globalConfig);

//数据源配置
       DataSourceConfig dataSourceConfig = new DataSourceConfig();

//mysql 数据库
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setUrl("jdbc:mysql://192.168.10.33:3306/magic_swift?characterEncoding=utf8&serverTimezone=UTC");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("mysql_OA0!");
       gen.setDataSource(dataSourceConfig);

//包配置
       PackageConfig packageConfig = new PackageConfig();
//packageConfig.setModuleName("anti_generator");
       packageConfig.setParent("com.jmyd.swift");
       gen.setPackageInfo(packageConfig);

//策略配置
       StrategyConfig strategyConfig = new StrategyConfig();
       strategyConfig.setNaming(NamingStrategy.underline_to_camel); //表名生成策略
       strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel); //列名生成策略
//lomok 模板
//strategyConfig.setEntityLombokModel(true);
//strategyConfig.setRestControllerStyle(true);
//是否生成注解@TableField
       strategyConfig.setEntityTableFieldAnnotationEnable(true);
//需要生成的表
       strategyConfig.setInclude("swift_info");
       gen.setStrategy(strategyConfig);
//自定义属性注入，这个必须有
       InjectionConfig cfg=new InjectionConfig() {
           @Override
           public void initMap() { }
       };
       gen.setCfg(cfg);
//执行生成
       gen.execute();
   }

}
