package yun.ling.pm.shardingjdbc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yun.ling.pm.shardingjdbc.mapper.TOrderMapper;
import yun.ling.pm.shardingjdbc.model.TOrder;

import java.util.List;

@Service
public class TestService {
    @Autowired
    private TOrderMapper tOrderMapper;

    public void insert(Integer id){
        TOrder tOrder = new TOrder();
        tOrder.setCode("1");
        tOrder.setName("2");
        tOrder.setRemark("3");
        tOrder.setId(id);
        tOrderMapper.insert(tOrder);
    }

    public List<TOrder> selectAll(TOrder tOrder){
        return tOrderMapper.select(tOrder);
    }

    public void delete(String id){
        tOrderMapper.deleteByIds(id);
    }
}
