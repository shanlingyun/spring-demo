package yun.ling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZKConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZKConsumerApplication.class,args);
    }

}
