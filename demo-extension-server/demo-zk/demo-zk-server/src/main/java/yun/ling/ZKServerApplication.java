package yun.ling;

import com.gitee.pifeng.monitoring.starter.annotation.EnableMonitoring;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableMonitoring
public class ZKServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZKServerApplication.class,args);
    }

}
