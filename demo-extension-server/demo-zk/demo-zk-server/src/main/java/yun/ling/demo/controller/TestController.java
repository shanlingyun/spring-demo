package yun.ling.demo.controller;

import com.google.gson.Gson;
import com.jmyd.jianmo.extension.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
public class TestController {

    @Resource
    private RedisService redisService;
    @Resource
    private RabbitTemplate rabbitTemplate;
    @Resource
    private StringRedisTemplate stringRedisTemplateOne;
    @Resource
    private StringRedisTemplate stringRedisTemplateTwo;
    @Resource
    private RedisTemplate redisTemplate;

    @GetMapping("hello")
    public String hello(String name,String key,String key2){
        Object object =null;
        if(StringUtils.isNotEmpty(key)) {
            object= redisService.getTemplateType(key);
        }
        log.info("redis template zero={}",object);
        log.info("redis template one={}",stringRedisTemplateOne.opsForValue().get(key));
        log.info("redis template two={}",stringRedisTemplateTwo.opsForValue().get(key));
        log.info("redis redisTemplate two={}",redisTemplate.opsForList().range(key2,0,-1));

        rabbitTemplate.convertAndSend("test",new Gson().toJson(object));
        return object!=null?new Gson().toJson(object):name;
    }
}
