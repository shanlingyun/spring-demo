# demo-getway

#### 介绍
spring微服务网关getway

#### 软件架构
 1、概述简介
 （1）简介
 SpringCloud Gateway是Spring Cloud的一个全新项目，纡Spring 5.0+ Spring Boot 2.0和Project Reactor等技术开发的网关,它旨在为微服务架构提供一种简单有效的统- 的API路由管理方式。
 SpringCloud Gateway作为Spring Cloud生态系统中的网关,目标是替代Zuul,在Spring Cloud 2.0以上版本中，没有对新版本的Zuul 2.0以上最新高性能版本进行集成，仍然还是使用的Zuul 1.x非Reactor模式的老版本。而为了提升网关的性能，SpringCloud Gateway是基于WebFlux框架实现的，而WebFlux框架底层则使用了高性能的Reactor模式通信框架Netty。
 Spring Cloud Gateway的目标提供统- -的路由方式且基于 Filter 链的方式提供了网关基本的功能，例如:安全，监控/指标, 和限流。
 一句话：springCloud Geteway使用的Webflux中的reactor-netty响应式变成组建，底层使用了Netty通讯框架。
 （2）能干嘛
 反向代理
 鉴权
 流量控制
 熔断
 日志监控等
 2、三大核心概念
 （1）Route 路由
 构建网关的基本模块，它由ID,目标URI，一系列的断言和过滤器组成，如果断言为true则匹配该路由
 （2）Predicate 断言
 参考的是Java8的java.util.function.Predicate 开发人员可以匹配HTTP请求中的所有内容(例如请求头或请求参数)，如果请求与断言相匹配则进行路由
 （3）Filter 过滤
 指的是Spring框架中GatewayFilter的实例，使用过滤器，可以在请求被路由前或者之后对请求进行修改。
 （4）总体
 web请求，通过一些匹配条件，定位到真正的服务节点。并在这个转发过程的前后，进行一些精细化控制。
 predicate就是我们的匹配条件;
 而filter,就可以理解为一个无所不能的拦截器。有了这两个元素,再加上目标uri,就可以实现一个具体的路由了
 3、Getway工作流程
 客户端向Spring Cloud Gatqway发出请求。然后在Gateway Handler Mapping中找到与请求相匹配的路由，将其发送到Gateway
 Web Handler。
 Handler再通过指定的过滤器链来将请求发送到我们实际的服务执行业务逻辑，然后返回。
 过滤器之间用虚线分开是因为过滤器可能会在发送代理请求之前( “pre” )或之后( “post” )执行业务逻辑。
 Filter在 “pre” 类型的过滤器可以做参数校验、权限校验、流量监控、日志输出、协议转换等,
 在"post" 类型的过滤器中可以做响应内容、响应头的修改,日志的输出，流量监控等有着非常重要的作用。
 核心逻辑：路由转发+执行过滤链 


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技
 