package yun.ling.pm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 模型数据统计
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@TableName("jianmo_model_count")
public class JianmoModelCountEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
      @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 创建时间
     */
    @TableField("CREATE_TS")
    private Date createTs;

    /**
     * 修改时间
     */
    @TableField("UPDATE_TS")
    private Date updateTs;

    /**
     * 模型编码
     */
    @TableField("MODEL_CODE")
    private String modelCode;

    /**
     * 模型分类CODE
     */
    @TableField("MODEL_TYPE_CODE")
    private String modelTypeCode;

    /**
     * 模型类型(1:obj,2:fbx,3:pic3d,8:stl,9:glb,10:gltf)
     */
    @TableField("MODEL_TYPE")
    private Integer modelType;

    /**
     * 业务分类(10、新增，20、浏览，30、编辑)
     */
    @TableField("TYPE")
    private Integer type;

    /**
     * 总记录数
     */
    @TableField("COUNT_NUM")
    private Integer countNum;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public Date getUpdateTs() {
        return updateTs;
    }

    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getModelTypeCode() {
        return modelTypeCode;
    }

    public void setModelTypeCode(String modelTypeCode) {
        this.modelTypeCode = modelTypeCode;
    }

    public Integer getModelType() {
        return modelType;
    }

    public void setModelType(Integer modelType) {
        this.modelType = modelType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCountNum() {
        return countNum;
    }

    public void setCountNum(Integer countNum) {
        this.countNum = countNum;
    }

    @Override
    public String toString() {
        return "JianmoModelCountEntity{" +
        "id=" + id +
        ", createTs=" + createTs +
        ", updateTs=" + updateTs +
        ", modelCode=" + modelCode +
        ", modelTypeCode=" + modelTypeCode +
        ", modelType=" + modelType +
        ", type=" + type +
        ", countNum=" + countNum +
        "}";
    }
}
