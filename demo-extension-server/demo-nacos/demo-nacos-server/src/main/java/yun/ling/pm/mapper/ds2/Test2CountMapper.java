package yun.ling.pm.mapper.ds2;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yun.ling.pm.entity.JianmoModelCountEntity;
import yun.ling.pm.vo.JianmoModelCountVO;

import java.util.List;

/**
 * <p>
 * 模型数据统计 Mapper 接口
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
public interface Test2CountMapper extends BaseMapper<JianmoModelCountEntity> {
    List<JianmoModelCountVO> test2();
}
