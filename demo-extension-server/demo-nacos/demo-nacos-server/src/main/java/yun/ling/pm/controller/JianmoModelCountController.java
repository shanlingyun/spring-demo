package yun.ling.pm.controller;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import yun.ling.pm.service.Test1CountService;
import yun.ling.pm.service.Test2ModelCountService;

import javax.annotation.Resource;

/**
 * <p>
 * 模型数据统计 前端控制器
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@RestController
@RequestMapping("/model")
public class JianmoModelCountController {

    @Resource
    private Test1CountService test1CountService;

    @Resource
    private Test2ModelCountService test2ModelCountService;

    @RequestMapping(value = "list" ,method = {RequestMethod.GET,RequestMethod.POST})
    public String insert(){
       test1CountService.test1().forEach(jianmoModelCountEntity -> {
            System.out.println("jianmoModelCountEntity.getModelCode() = " + jianmoModelCountEntity.getModelCode());
        });


        test2ModelCountService.test2().forEach(jianmoModelCountEntity -> {
            System.out.println("jianmoModelCountEntity.getModelCode() = " + jianmoModelCountEntity.getModelCode());
        });
        return "";
    }

}

