package yun.ling.pm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import yun.ling.pm.entity.JianmoModelCountEntity;
import yun.ling.pm.mapper.ds2.Test2CountMapper;
import yun.ling.pm.service.Test2ModelCountService;
import yun.ling.pm.vo.JianmoModelCountVO;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 模型数据统计 服务实现类
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@Service
public class Test2ModelCountServiceImpl extends ServiceImpl<Test2CountMapper, JianmoModelCountEntity> implements Test2ModelCountService {

    @Resource
    private Test2CountMapper test2CountMapper;

    @Override
    public List<JianmoModelCountVO> test2() {
        return test2CountMapper.test2();
    }
}
