package yun.ling.pm.service.impl;

import yun.ling.pm.entity.JianmoModelCountEntity;
import yun.ling.pm.mapper.ds1.Test1Mapper;
import yun.ling.pm.service.Test1CountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import yun.ling.pm.vo.JianmoModelCountVO;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 模型数据统计 服务实现类
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@Service
public class Test1CountServiceImpl extends ServiceImpl<Test1Mapper, JianmoModelCountEntity> implements Test1CountService {

    @Resource
    private Test1Mapper test1Mapper;

    @Override
    public List<JianmoModelCountVO> test1() {
        return test1Mapper.test1();
    }
}
