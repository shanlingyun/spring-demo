package yun.ling.pm.vo;

import java.io.Serializable;

public class SceneScene implements Serializable {

    private  String id;
    private String version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
