package yun.ling.pm.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 模型数据统计
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
@Data
public class JianmoModelCountVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 创建时间
     */
    private Date createTs;

    /**
     * 修改时间
     */
    private Date updateTs;

    /**
     * 模型编码
     */
    private String modelCode;

    /**
     * 模型分类CODE
     */
    private String modelTypeCode;

    /**
     * 模型类型(1:obj,2:fbx,3:pic3d,8:stl,9:glb,10:gltf)
     */
    private Integer modelType;

    /**
     * 业务分类(10、新增，20、浏览，30、编辑)
     */
    private Integer type;

    /**
     * 总记录数
     */
    private Integer countNum;

}
