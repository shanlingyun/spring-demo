package yun.ling;

import com.gitee.pifeng.monitoring.starter.annotation.EnableMonitoring;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
@EnableMonitoring
public class NacosServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosServerApplication.class,args);
    }

}
