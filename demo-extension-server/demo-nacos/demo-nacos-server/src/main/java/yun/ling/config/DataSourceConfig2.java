package yun.ling.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "yun.ling.pm.mapper.ds2",sqlSessionFactoryRef = "sqlSessionFactory2")
public class DataSourceConfig2 {
    @Bean
    @ConfigurationProperties("spring.datasource2")
    public DataSourceProperties dataSourceProperties2(){
        return new DataSourceProperties();
    }


    @Bean("dataSource2")
    public DataSource dataSource2(){
        DataSourceProperties dataSourceProperties = dataSourceProperties2();
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean("sqlSessionFactory2")
    public SqlSessionFactory sqlSessionFactory2(@Qualifier("dataSource2")DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource);
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().
                getResources("classpath*:yun/ling/pm/mapper/ds2/*.xml"));
        return sqlSessionFactory.getObject();
    }

    @Bean("txTransManager2")
    @Resource
    public PlatformTransactionManager txTransManager2(@Qualifier("dataSource2")DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean("sqlSessionTemplate2")
    @Resource
    public SqlSessionTemplate sqlSessionTemplate2(@Qualifier("sqlSessionFactory2")SqlSessionFactory sqlSessionFactory){
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
