package yun.ling.pm.dao;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import yun.ling.pm.vo.JianmoModelCountVO;

import java.util.List;

@Mapper
public interface SceneSceneMapper {
    @Select("SELECT ID, CREATE_TS, UPDATE_TS, MODEL_CODE as modelCode, MODEL_TYPE_CODE, MODEL_TYPE, TYPE, COUNT_NUM FROM jianmo_model_count LIMIT 20")
    List<JianmoModelCountVO> select();

}
