package yun.ling.pm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yun.ling.pm.entity.JianmoModelCountEntity;
import yun.ling.pm.vo.JianmoModelCountVO;

import java.util.List;

/**
 * <p>
 * 模型数据统计 服务类
 * </p>
 *
 * @author shanlingyun
 * @since 2022-05-06
 */
public interface Test2ModelCountService extends IService<JianmoModelCountEntity> {
    List<JianmoModelCountVO> test2();

}
