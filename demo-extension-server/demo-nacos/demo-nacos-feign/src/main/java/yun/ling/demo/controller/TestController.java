package yun.ling.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import yun.ling.demo.service.HelloService;

@RestController
public class TestController {

    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "consumer",method = RequestMethod.GET)
    public String hello(String name){
        String str = helloService.hello(name);
        return str;
    }
}
