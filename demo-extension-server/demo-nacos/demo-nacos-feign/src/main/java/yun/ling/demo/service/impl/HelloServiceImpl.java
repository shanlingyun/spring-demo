package yun.ling.demo.service.impl;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import yun.ling.demo.service.HelloService;

@Component
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String name) {
        return "哈哈";
    }

}
