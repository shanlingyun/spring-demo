package yun.ling.demo.service;

 import org.springframework.cloud.openfeign.FeignClient;
 import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 import org.springframework.web.bind.annotation.RequestParam;
 import yun.ling.demo.service.impl.HelloServiceImpl;

@FeignClient(value = "demo-nacos-server",fallback = HelloServiceImpl.class)
public interface HelloService {
    @RequestMapping(value = "hello",method = RequestMethod.GET)
    String hello(@RequestParam(name = "name")String name);
}
