package yun.ling.demo.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

public class AESUtil {

    public static String getRandomChars(int length) {
        String randomChars = "";
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < length; i++) {
                randomChars += String.valueOf(random.nextInt(2));
        }
        return randomChars;
    }
        /**
         * 加密
         *
         * @param content
         * @param strKey
         * @return
         * @throws Exception
         */
        public static byte[] encrypt(String content, String strKey,String iv) throws Exception {
            SecretKeySpec skeySpec = getKey(strKey);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec,  new IvParameterSpec(iv.getBytes()));
            return cipher.doFinal(content.getBytes());
        }

        /**
         * 解密
         *
         * @param strKey
         * @param content
         * @return
         * @throws Exception
         */
        public static String decrypt(byte[] content, String strKey,String iv) throws Exception {
            SecretKeySpec skeySpec = getKey(strKey);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(iv.getBytes()));
            byte[] original = cipher.doFinal(content);
            String originalString = new String(original);
            return originalString;
        }

        private static SecretKeySpec getKey(String strKey) throws Exception {
            byte[] arrBTmp = strKey.getBytes();
            byte[] arrB = new byte[16];
            for (int i = 0; i < arrBTmp.length && i < arrB.length; i++) {
                arrB[i] = arrBTmp[i];
            }
            SecretKeySpec skeySpec = new SecretKeySpec(arrB, "AES");
            return skeySpec;
        }

    public static void main(String[] args) throws Exception {
            String content ="shanlingyun123456789";
            String key="jmyd";
            String iv =getRandomChars(16);
        byte byt[] = encrypt(content,key,iv);
        String dcont =decrypt(byt,key,iv);
        System.out.println(dcont);
    }
}
