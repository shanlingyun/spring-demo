package yun.ling.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;
    private String url="http://demo-nacos-server";

    @GetMapping("consumer")
    public String hello(String name){
        return restTemplate.getForObject(url+"/insert?name="+name,String.class);
    }


    public   String generateSaaSUsernameOrPwd(String key, String str, int encryptLength) {
        String iv = getRandomChars(16);
        String afterEncryptStr = "";
        try {
            afterEncryptStr = encryptAESCBCEncode(str, key, iv, encryptLength);
        }catch (InvalidKeyException | NoSuchAlgorithmException
                | NoSuchPaddingException | InvalidAlgorithmParameterException
                | IllegalBlockSizeException | BadPaddingException e) {
        }
        System.out.println(afterEncryptStr);
        return iv + afterEncryptStr;

    }

        /**
         *
         * 字节数组转字符串
         * @param bytes 字节数组
         * @return 字符串
         */
        public static String base_64(byte[] bytes) {
            return new String(org.apache.commons.codec.binary.Base64.encodeBase64(bytes));
        }



        /**
         * 随机生成字符串
         * @param length 随机字符串的长度
         * @return 随机字符串
         */

        public static String getRandomChars(int length) {
            String randomChars = "";
            SecureRandom random = new SecureRandom();
            for (int i = 0; i < length; i++) {
                //字母和数字中随机
                if (random.nextInt(2) % 2 == 0) {
                    //输出是大写字母还是小写字母
                    int letterIndex = random.nextInt(2) % 2 == 0 ? 65 : 97;
                    randomChars += (char) (random.nextInt(26) + letterIndex);
                } else {
                    randomChars += String.valueOf(random.nextInt(10));
                }
            }
            return randomChars;
        }

        /**

         * AES CBC 位加密
         * @param content 加密内容
         * @param key 加密秘钥
         * @param iv 向量iv
         * @param encryptLength 仅支持128、256长度
         * @return 加密结果
         * @throws BadPaddingException
         * @throws IllegalBlockSizeException
         * @throws InvalidAlgorithmParameterException
         * @throws NoSuchPaddingException
         * @throws NoSuchAlgorithmException
         * @throws InvalidKeyException
         */

        public static String encryptAESCBCEncode(String content, String key,
                                                 String iv, int encryptLength)
                throws InvalidKeyException, NoSuchAlgorithmException,
                NoSuchPaddingException, InvalidAlgorithmParameterException,
                IllegalBlockSizeException, BadPaddingException {
            if (StringUtils.isEmpty(content) || StringUtils.isEmpty(key)
                    || StringUtils.isEmpty(iv)) {
                return null;
            }
            return base_64(
                    encryptAESCBC(content.getBytes(), key.getBytes(), iv.getBytes(), encryptLength));
        }

        /**
         *
         * AES CBC 256位加密
         * @param content 加密内容字节数组
         * @param keyBytes 加密字节数组
         * @param iv 加密向量字节数组
         * @param encryptLength 仅支持128、256长度
         * @return 解密后字节内容
         * @throws NoSuchAlgorithmException
         * @throws NoSuchPaddingException
         * @throws InvalidKeyException
         * @throws InvalidAlgorithmParameterException
         * @throws IllegalBlockSizeException
         * @throws BadPaddingException
         */

        public static byte[] encryptAESCBC(byte[] content, byte[] keyBytes,
                                           byte[] iv, int encryptLength)
                throws NoSuchAlgorithmException, NoSuchPaddingException,
                InvalidKeyException, InvalidAlgorithmParameterException,
                IllegalBlockSizeException, BadPaddingException {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(keyBytes);
            keyGenerator.init(encryptLength, secureRandom);
            SecretKey key = keyGenerator.generateKey();
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
            byte[] result = cipher.doFinal(content);
            return result;
        }


    private static String PUBLIC_KEY="public";
    private static String PRIVATE_KEY="privatekey";
    private static String algorithm="RSA";

    public static String decryptByPrivateKey(String data, String privateKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateK);
        byte[] buff = cipher.doFinal(Base64.decodeBase64(data));
        return new String(buff);
    }


    public static String encryptByPublicKey(String data, String publicKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(publicKey);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Key publicK = keyFactory.generatePublic(x509KeySpec);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicK);
        byte[] buff = cipher.doFinal(data.getBytes());
        return Base64.encodeBase64String(buff);
    }

    public static Map<String, Object> initKey() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(algorithm);
        keyPairGen.initialize(1024);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        Map<String, Object> keyMap = new HashMap<String, Object>(2);
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;
    }


    public static String encrypt(String iv) throws Exception {
        try {
            String data = "我们都是好孩子";
            String key = "1q2w3e4r5t";
            //String iv = "7sI0154q4N7t180t";

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);
            return new sun.misc.BASE64Encoder().encode(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
        /**
         * 解密AES CBC
         * @param content 原文
         * @param key 秘钥
         * @param iv 盐值
         * @return 解密结果
         * @throws BadPaddingException
         * @throws IllegalBlockSizeException
         * @throws InvalidAlgorithmParameterException
         * @throws NoSuchPaddingException
         * @throws NoSuchAlgorithmException
         * @throws InvalidKeyException
         */

        public static String decryptAESCBCEncode(String content, String key,

                                                 String iv, int encryptType)
                throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException,
                IllegalBlockSizeException, BadPaddingException {
            if (StringUtils.isEmpty(content) || StringUtils.isEmpty(key)
                    || StringUtils.isEmpty(iv)){
                return null;
            }
            return new String(decryptAESCBC(Base64.decodeBase64(content.getBytes()),
                    key.getBytes(),
                    iv.getBytes(),encryptType));
        }

        public static byte[] decryptAESCBC(byte[] content, byte[] keyBytes,
                                           byte[] iv, int encryptType)
                throws NoSuchAlgorithmException,NoSuchPaddingException,InvalidKeyException,
                InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(keyBytes);
            keyGenerator.init(encryptType, secureRandom);
            SecretKey key = keyGenerator.generateKey();
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            byte[] result = cipher.doFinal(content);
            return result;
        }

    public static void main(String[] args) throws Exception {
      /*  String content="我们都是好孩子";
        String key="1q2w3e4r5t";
        String iv = getRandomChars(16);
        int encryptType=256;
       String enc= iv+encryptAESCBCEncode(content, key,iv ,encryptType);
        System.out.println(enc);
       String abc= encrypt(iv);
        System.out.println(abc);
       String iv2 = enc.substring(0,16);
        String dec= decryptAESCBCEncode(enc.substring(16), key,iv2 ,encryptType);
        System.out.println(dec);*/
      String data="sly";
      Map<String ,Object> keyMap =initKey();
             Key key = (Key) keyMap.get(PUBLIC_KEY);
         String result =encryptByPublicKey(data,Base64.encodeBase64String(key.getEncoded()));
        System.out.println(result);
        Key key2 = (Key) keyMap.get(PRIVATE_KEY);

        String old=decryptByPrivateKey(result,Base64.encodeBase64String(key2.getEncoded()));
        System.out.println(old);
    }

}
