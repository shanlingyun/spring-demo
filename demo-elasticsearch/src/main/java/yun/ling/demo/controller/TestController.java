package yun.ling.demo.controller;

import com.google.gson.Gson;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import yun.ling.demo.controller.manager.ElasManager;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
public class TestController {
    @Resource
    private ElasManager elasManager;

    @GetMapping("consumer")
    public String hello(String name) throws IOException {
           return new Gson().toJson(elasManager.test());
    }
}
