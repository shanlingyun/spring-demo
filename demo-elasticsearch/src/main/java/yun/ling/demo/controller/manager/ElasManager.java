package yun.ling.demo.controller.manager;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;
import yun.ling.demo.controller.dao.EmployeeRepository;
import yun.ling.demo.controller.vo.Employee;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class ElasManager {

    @Resource
    private RestHighLevelClient restHighLevelClient;
    @Autowired
    private EmployeeRepository employeeRepository;

    public Map test(){
        Map map = new HashMap();
        map.put("1","2");
        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("haha");
        employeeRepository.save(employee);
        //employeeRepository.delete(employee);
        GetIndexResponse response =null;
                GetIndexRequest request = new GetIndexRequest("shanlingyun");
        try {
            response =  restHighLevelClient.indices().get(request, RequestOptions.DEFAULT);
            return response.getMappings();
        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }

    public void test2(){
        Pageable pageable =  PageRequest.of(0, 20);
        NativeSearchQueryBuilder searchQueryBuilder =  new  NativeSearchQueryBuilder().withPageable(pageable);
     }
}
