package yun.ling.demo.controller.vo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Data
@Accessors(chain =  true)
@Document(indexName =  "shanlingyun", type =  "yun.ling.demo.controller.vo.Employee")
public class Employee implements Serializable {

    @Id
    private Integer id;
    private String name;
}
