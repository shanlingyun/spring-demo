package yun.ling.demo.controller.dao;

import org.mapstruct.Mapper;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;
import yun.ling.demo.controller.vo.Employee;

public interface EmployeeRepository extends ElasticsearchRepository<Employee,String> {
}
