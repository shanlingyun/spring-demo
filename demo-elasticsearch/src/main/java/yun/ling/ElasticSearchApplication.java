package yun.ling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
 public class ElasticSearchApplication {

    public static void main(String[] args) {
        new SpringApplication().run(ElasticSearchApplication.class,args);
    }
}
