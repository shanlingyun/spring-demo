package yun.ling.demo.job;

import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.elasticjob.api.ShardingContext;
import org.apache.shardingsphere.elasticjob.simple.job.SimpleJob;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class My3ElasticJob implements SimpleJob {
    private Integer count=0;
    @Override
    public void execute(ShardingContext shardingContext) {
        log.error("My3ElasticJob,start"+count++);
    }
}
