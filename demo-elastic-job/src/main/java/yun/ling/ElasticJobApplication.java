package yun.ling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
 public class ElasticJobApplication {

    public static void main(String[] args) {
        new SpringApplication().run(ElasticJobApplication.class,args);
    }
}
