package yun.ling.config;

 import org.apache.shardingsphere.elasticjob.api.ElasticJob;
 import org.apache.shardingsphere.elasticjob.api.JobConfiguration;
 import org.apache.shardingsphere.elasticjob.lite.api.bootstrap.impl.ScheduleJobBootstrap;
 import org.apache.shardingsphere.elasticjob.reg.zookeeper.ZookeeperRegistryCenter;
 import org.apache.shardingsphere.elasticjob.simple.job.SimpleJob;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
 import java.util.Arrays;
 import java.util.Map;

public class SimpleJobAutoConfig {
    @Resource
    private ZookeeperRegistryCenter zookeeperRegistryCenter;
    @Resource
    private ApplicationContext applicationContext;
/*
    public void initSimpleJob() {
        Map<String, Object> beans = applicationContext.getBeansWithAnnotation(ElasticSimpleJob.class);
        Arrays.asList(beans.entrySet()).forEach( instance -> {
             Class<?>[] interfaces = instance.getClass().getInterfaces();
            Arrays.asList(interfaces).forEach(anInterface->{
                if (anInterface == SimpleJob.class) {
                    ElasticSimpleJob annotation = instance.getClass().getAnnotation(ElasticSimpleJob.class);
                    String jobName = annotation.jobName();
                    String cron = annotation.cron();
                    boolean overwrite = annotation.overwrite();
                    int shardingTotalCount = annotation.shardingTotalCount();
                    JobConfiguration jobConfiguration = JobConfiguration.newBuilder(jobName,shardingTotalCount)
                            .cron(cron).overwrite(overwrite).build();
                    new ScheduleJobBootstrap(zookeeperRegistryCenter, (ElasticJob)instance,jobConfiguration).schedule();
                }
            });
        });
    }*/

}
