package yun.ling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@SpringBootApplication
@RestController
//@EnableDiscoveryClient
public class GetWayApplication {

    @RequestMapping("demo-getway")
    public String hystrixfallback() {
        return "This is a fallback";
    }

    @RequestMapping("test2")
    public String test2() {
        return "This is a fallback："+new Date().getTime();
    }

    public static void main(String[] args) {
        SpringApplication.run(GetWayApplication.class,args);
    }

}
