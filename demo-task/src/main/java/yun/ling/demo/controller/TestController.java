package yun.ling.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;
    private String url="http://demo-nacos-server";

    @GetMapping("consumer")
    public String hello(String name){
        return restTemplate.getForObject(url+"/hello?name="+name,String.class);
    }
}
