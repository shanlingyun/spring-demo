package yun.ling.demo.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class TimeJobTask {

    @Scheduled(cron = "0 */30 * * * ?")
    public void say(){
        log.error("时间："+new Date());
    }
}
