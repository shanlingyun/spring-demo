package yun.ling.demo.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.task.listener.TaskExecutionListener;
import org.springframework.cloud.task.repository.TaskExecution;

@Slf4j
public class TaskListener implements TaskExecutionListener {
    @Override
    public void onTaskStartup(TaskExecution taskExecution) {
      log.debug("任务开始");
    }

    @Override
    public void onTaskEnd(TaskExecution taskExecution) {
        log.error("任务结束");
    }

    @Override
    public void onTaskFailed(TaskExecution taskExecution, Throwable throwable) {
        log.error("任务失败");
    }


}
